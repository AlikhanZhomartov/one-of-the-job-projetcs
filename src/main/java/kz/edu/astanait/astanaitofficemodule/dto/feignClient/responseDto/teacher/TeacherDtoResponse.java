package kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.teacher;

import lombok.Data;

@Data
public class TeacherDtoResponse {

    private String nameRu;

    private String surnameRu;

    private String patronymicRu;

    private Long departmentId;
}
