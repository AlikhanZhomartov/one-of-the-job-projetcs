package kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JWTDtoResponse {
    Long userId;
    String[] authorities;
}
