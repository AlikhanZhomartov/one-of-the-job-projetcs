package kz.edu.astanait.astanaitofficemodule.service.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.ResultOfEducationProgram.ResultOfEducationProgramDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.ResultOfEducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.CustomNotFoundException;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.ResultOfEducationProgramMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.EducationProgram;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.ResultOfEducationProgram;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.EducationProgramRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.ResultOfEducationProgramRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ResultOfEducationProgramService {

    private final ResultOfEducationProgramRepository resultOfEducationProgramRepository;
    private final EducationProgramRepository educationProgramRepository;

    @Autowired
    public ResultOfEducationProgramService(ResultOfEducationProgramRepository resultOfEducationProgramRepository, EducationProgramRepository educationProgramRepository) {
        this.resultOfEducationProgramRepository = resultOfEducationProgramRepository;
        this.educationProgramRepository = educationProgramRepository;
    }

    public void createResultOfEducationProgram(ResultOfEducationProgramDtoRequest resultOfEducationProgramDtoRequests) {
        EducationProgram educationProgram = educationProgramRepository.findById(resultOfEducationProgramDtoRequests.getEducationProgramId()).orElseThrow(
                () -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Education program", "id")));
            manipulateResultOfEducationProgram(new ResultOfEducationProgram(), resultOfEducationProgramDtoRequests, educationProgram, TypeOfActionForHistory.CREATE);
    }

    public void updateResultOfEducationProgram(ResultOfEducationProgramDtoRequest resultOfEducationProgramDtoRequests, Long resultOfEducationProgramId) {
        EducationProgram educationProgram = educationProgramRepository.findById(resultOfEducationProgramDtoRequests.getEducationProgramId()).orElseThrow(
                () -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Education program", "id")));
            Optional<ResultOfEducationProgram> resultOfEducationProgram = resultOfEducationProgramRepository.findById(resultOfEducationProgramId);
            resultOfEducationProgram.ifPresent(v -> manipulateResultOfEducationProgram(v, resultOfEducationProgramDtoRequests, educationProgram, TypeOfActionForHistory.UPDATE));
    }

    public void deleteResultOfEducationProgram(Long educationProgramId) {
        try {
            resultOfEducationProgramRepository.deleteByEducationProgramId(educationProgramId);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "result of education program", TypeOfActionForHistory.DELETE));
        }
    }
    private void manipulateResultOfEducationProgram(ResultOfEducationProgram resultOfEducationProgram, ResultOfEducationProgramDtoRequest resultOfEducationProgramDtoRequest, EducationProgram educationProgram, String Action) {
        if (Strings.isNotBlank(resultOfEducationProgramDtoRequest.getResultEn())) resultOfEducationProgram.setResultEn(resultOfEducationProgramDtoRequest.getResultEn());
        if (Strings.isNotBlank(resultOfEducationProgramDtoRequest.getResultRu())) resultOfEducationProgram.setResultRu(resultOfEducationProgramDtoRequest.getResultRu());
        if (Strings.isNotBlank(resultOfEducationProgramDtoRequest.getResultKz())) resultOfEducationProgram.setResultKz(resultOfEducationProgramDtoRequest.getResultKz());
        if (Strings.isNotBlank(resultOfEducationProgramDtoRequest.getCode())) resultOfEducationProgram.setCode(resultOfEducationProgramDtoRequest.getCode());
        resultOfEducationProgram.setEducationProgram(educationProgram);
        try {
            resultOfEducationProgramRepository.save(resultOfEducationProgram);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "result of education program", Action));
        }
    }

    public List<ResultOfEducationProgramDtoResponse> getResultOfEducationProgram(Long educationProgram) {
        ResultOfEducationProgramMapper resultOfEducationProgramMapper = new ResultOfEducationProgramMapper();
        return resultOfEducationProgramRepository.findByEducationProgramId(educationProgram)
                .stream().map(resultOfEducationProgramMapper::resultOfEducationProgramToDto).collect(Collectors.toList());
    }
}
