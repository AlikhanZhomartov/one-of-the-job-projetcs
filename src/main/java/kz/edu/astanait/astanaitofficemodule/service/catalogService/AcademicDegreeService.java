package kz.edu.astanait.astanaitofficemodule.service.catalogService;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.AcademicDegreeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.AcademicDegreeMapper;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.AcademicDegreeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AcademicDegreeService {

    private final AcademicDegreeRepository academicDegreeRepository;

    @Autowired
    public AcademicDegreeService(AcademicDegreeRepository academicDegreeRepository) {
        this.academicDegreeRepository = academicDegreeRepository;
    }

    public List<AcademicDegreeDtoResponse> getAllAcademicDegrees() {
        AcademicDegreeMapper academicDegreeMapper = new AcademicDegreeMapper();
        return academicDegreeRepository.findAll().stream().map(academicDegreeMapper::academicDegreeToDto).collect(Collectors.toList());
    }
}
