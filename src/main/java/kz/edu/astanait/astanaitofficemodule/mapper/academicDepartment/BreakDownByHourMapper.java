package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.BreakDownByHourDtoResponse;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.LessonsTypeMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.BreakDownByHour;

public class BreakDownByHourMapper {

    public BreakDownByHourDtoResponse downByHourToDto(BreakDownByHour breakDownByHour) {
        BreakDownByHourDtoResponse breakDownByHourDtoResponse = new BreakDownByHourDtoResponse();
        breakDownByHourDtoResponse.setId(breakDownByHour.getId());
        breakDownByHourDtoResponse.setDisciplineId(breakDownByHour.getDiscipline().getId());
        breakDownByHourDtoResponse.setLessonsTypeId(new LessonsTypeMapper().lessonsTypeToDto(breakDownByHour.getLessonsType()));
        breakDownByHourDtoResponse.setHours(breakDownByHour.getHours());
        return breakDownByHourDtoResponse;
    }
}
