package kz.edu.astanait.astanaitofficemodule.controller.auth;

import kz.edu.astanait.astanaitofficemodule.exception.ExceptionHandling;
import kz.edu.astanait.astanaitofficemodule.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/v1/auth")
public class AuthController extends ExceptionHandling {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/register")
    public ResponseEntity<HttpStatus> registerStudent(@RequestParam Long userId) {
        authService.registerStudent(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
