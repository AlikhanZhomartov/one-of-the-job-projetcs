package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.ResultOfEducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.ResultOfEducationProgram;
import org.apache.logging.log4j.util.Strings;

public class ResultOfEducationProgramMapper {

    public ResultOfEducationProgramDtoResponse resultOfEducationProgramToDto(ResultOfEducationProgram resultOfEducationProgram) {
        ResultOfEducationProgramDtoResponse resultOfEducationProgramDtoResponse = new ResultOfEducationProgramDtoResponse();
        resultOfEducationProgramDtoResponse.setId(resultOfEducationProgram.getId());
        if (Strings.isNotBlank(resultOfEducationProgram.getResultEn())) resultOfEducationProgramDtoResponse.setResultEn(resultOfEducationProgram.getResultEn());
        if (Strings.isNotBlank(resultOfEducationProgram.getResultRu())) resultOfEducationProgramDtoResponse.setResultRu(resultOfEducationProgram.getResultRu());
        if (Strings.isNotBlank(resultOfEducationProgram.getResultKz())) resultOfEducationProgramDtoResponse.setResultKz(resultOfEducationProgram.getResultKz());
        if (Strings.isNotBlank(resultOfEducationProgram.getCode())) resultOfEducationProgramDtoResponse.setCode(resultOfEducationProgram.getCode());
        return resultOfEducationProgramDtoResponse;
    }
}
