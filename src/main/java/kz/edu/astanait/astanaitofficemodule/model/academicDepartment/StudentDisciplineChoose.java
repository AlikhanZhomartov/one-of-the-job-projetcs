package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "student_discipline_choose")
public class StudentDisciplineChoose {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "curriculum_id")
    private Curriculum curriculumId;

    private Integer priority;

    @Column(name = "student_id")
    private Long studentId;
}
