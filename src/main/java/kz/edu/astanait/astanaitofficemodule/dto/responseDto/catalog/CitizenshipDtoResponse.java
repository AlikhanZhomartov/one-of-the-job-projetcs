package kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog;

import lombok.Data;

@Data
public class CitizenshipDtoResponse {
    private Long id;

    private String titleEn;

    private String titleRu;

    private String titleKz;
}
