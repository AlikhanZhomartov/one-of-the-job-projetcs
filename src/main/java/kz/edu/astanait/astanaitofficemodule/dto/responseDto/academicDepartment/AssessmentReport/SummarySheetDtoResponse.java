package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AssessmentReport;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AcademicStreamDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AcademicStreamStudentDtoResponse;
import lombok.Data;

import java.util.List;

@Data
public class SummarySheetDtoResponse {

    private AcademicStreamDtoResponse academicStream;

    private List<AcademicStreamStudentDtoResponse> academicStreamStudent;

}
