package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import lombok.Data;

@Data
public class ResultOfEducationProgramDtoResponse {
    private Long id;

    private String resultEn;

    private String resultRu;

    private String resultKz;

    private String code;
}
