package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.teacher.TeacherDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AcademicStreamDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.LessonsTypeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.LessonsTypeMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.AcademicStream;
import org.apache.logging.log4j.util.Strings;

import java.util.Objects;

public class AcademicStreamMapper {

    public AcademicStreamDtoResponse academicStreamToDto(AcademicStream academicStream, TeacherDtoResponse teacherDtoResponse) {
        AcademicStreamDtoResponse academicStreamDtoResponse = new AcademicStreamDtoResponse();
        academicStreamDtoResponse.setTeacher(teacherDtoResponse);
        if (Objects.nonNull(academicStream.getDiscipline())) academicStreamDtoResponse.setDiscipline(new DisciplineMapper().disciplineToDto(academicStream.getDiscipline()));
        if (Objects.nonNull(academicStream.getYear())) academicStreamDtoResponse.setYear(academicStream.getYear());
        if (Objects.nonNull(academicStream.getSenior())) academicStreamDtoResponse.setSenior(academicStream.getSenior());
        if (Objects.nonNull(academicStream.getStreamStartedDate())) academicStreamDtoResponse.setStreamStartedDate(academicStream.getStreamStartedDate());
        if (Objects.nonNull(academicStream.getStreamEndedDate())) academicStreamDtoResponse.setStreamEndedDate(academicStream.getStreamEndedDate());
        if (Objects.nonNull(academicStream.getTerm())) academicStreamDtoResponse.setTerm(academicStream.getTerm());
        if (Strings.isNotBlank(academicStream.getStreamName())) academicStreamDtoResponse.setStreamName(academicStream.getStreamName());
        if (Objects.nonNull(academicStream.getStreamType())) academicStreamDtoResponse.setStreamType(new LessonsTypeMapper().lessonsTypeToDto(academicStream.getStreamType()));
        return academicStreamDtoResponse;
    }

    public AcademicStreamDtoResponse academicStreamToDtoWithoutTeacher(AcademicStream academicStream) {
        AcademicStreamDtoResponse academicStreamDtoResponse = new AcademicStreamDtoResponse();
        if (Objects.nonNull(academicStream.getDiscipline())) academicStreamDtoResponse.setDiscipline(new DisciplineMapper().disciplineToDto(academicStream.getDiscipline()));
        if (Objects.nonNull(academicStream.getYear())) academicStreamDtoResponse.setYear(academicStream.getYear());
        if (Objects.nonNull(academicStream.getSenior())) academicStreamDtoResponse.setSenior(academicStream.getSenior());
        if (Objects.nonNull(academicStream.getStreamStartedDate())) academicStreamDtoResponse.setStreamStartedDate(academicStream.getStreamStartedDate());
        if (Objects.nonNull(academicStream.getStreamEndedDate())) academicStreamDtoResponse.setStreamEndedDate(academicStream.getStreamEndedDate());
        if (Objects.nonNull(academicStream.getTerm())) academicStreamDtoResponse.setTerm(academicStream.getTerm());
        if (Strings.isNotBlank(academicStream.getStreamName())) academicStreamDtoResponse.setStreamName(academicStream.getStreamName());
        if (Objects.nonNull(academicStream.getStreamType())) academicStreamDtoResponse.setStreamType(new LessonsTypeMapper().lessonsTypeToDto(academicStream.getStreamType()));
        return academicStreamDtoResponse;
    }
}
