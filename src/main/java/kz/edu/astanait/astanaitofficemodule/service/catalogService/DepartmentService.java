package kz.edu.astanait.astanaitofficemodule.service.catalogService;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.DepartmentDtoResponse;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.DepartmentMapper;
import kz.edu.astanait.astanaitofficemodule.model.catalog.Department;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.DepartmentRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public List<DepartmentDtoResponse> getAllDepartments() {
        DepartmentMapper departmentMapper = new DepartmentMapper();
        return departmentRepository.findAll().stream().map(departmentMapper::departmentToDto).collect(Collectors.toList());
    }
}
