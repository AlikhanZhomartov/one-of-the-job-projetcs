package kz.edu.astanait.astanaitofficemodule.model.catalog;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "component")
public class Component {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "component_title_en")
    private String componentTitleEn;

    @Column(name = "component_title_ru")
    private String componentTitleRu;

    @Column(name = "component_title_kz")
    private String componentTitleKz;

    @Column(name = "component_abbreviation_en")
    private String componentAbbreviationEn;

    @Column(name = "component_abbreviation_ru")
    private String componentAbbreviationRu;

    @Column(name = "component_abbreviation_kz")
    private String componentAbbreviationKz;
}
