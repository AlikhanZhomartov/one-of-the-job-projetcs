package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "fx_grades")
public class FxGrades {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "academic_stream_student_id")
    private AcademicStreamStudent academicStreamStudent;

    @Column(name = "grade")
    private Float grade;
}
