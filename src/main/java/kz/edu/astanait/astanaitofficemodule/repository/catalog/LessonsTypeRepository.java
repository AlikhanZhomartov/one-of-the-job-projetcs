package kz.edu.astanait.astanaitofficemodule.repository.catalog;

import kz.edu.astanait.astanaitofficemodule.model.catalog.LessonsType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonsTypeRepository extends JpaRepository<LessonsType, Long> {
}
