package kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.breakDownByHour;

import lombok.Data;

@Data
public class UpdateBreakDownByHourDtoRequest {
    private Long breakDownByHourId;

    private Long lessonsTypeId;

    private Integer hours;
}
