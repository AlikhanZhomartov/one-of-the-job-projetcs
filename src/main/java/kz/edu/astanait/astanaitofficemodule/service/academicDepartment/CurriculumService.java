package kz.edu.astanait.astanaitofficemodule.service.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.Curriculum.CurriculumByGroupDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.Curriculum.CurriculumDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.CurriculumDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.CustomNotFoundException;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.CurriculumMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Curriculum;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Discipline;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.CurriculumRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.DisciplineRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.EducationProgramRepository;
import kz.edu.astanait.astanaitofficemodule.serviceHistory.academicDepartmentHistory.CurriculumHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CurriculumService {

    private final CurriculumRepository curriculumRepository;
    private final EducationProgramRepository educationProgramRepository;
    private final DisciplineRepository disciplineRepository;
    private final CurriculumHistoryService curriculumHistoryService;

    @Autowired
    public CurriculumService(CurriculumRepository curriculumRepository, EducationProgramRepository educationProgramRepository, DisciplineRepository disciplineRepository, CurriculumHistoryService curriculumHistoryService) {
        this.curriculumRepository = curriculumRepository;
        this.educationProgramRepository = educationProgramRepository;
        this.disciplineRepository = disciplineRepository;
        this.curriculumHistoryService = curriculumHistoryService;
    }

    public List<CurriculumDtoResponse> getAllCurriculums() {
        CurriculumMapper curriculumMapper = new CurriculumMapper();
        return curriculumRepository.findAll().stream().map(curriculumMapper::curriculumToDto).collect(Collectors.toList());
    }

    public List<CurriculumDtoResponse> getCurriculumsByEducationProgram(Long educationProgramId) {
        CurriculumMapper curriculumMapper = new CurriculumMapper();
        return curriculumRepository.findByEducationProgramId(educationProgramId).stream().map(curriculumMapper::curriculumToDto).collect(Collectors.toList());
    }

    public void createCurriculumByGroup(CurriculumByGroupDtoRequest curriculumByGroupDtoRequest, Long userId) {
        Long groupedDisciplinesNumber = curriculumRepository.findMaxValueOfGroupedDiscipline() + 1;
        for (Long disciplineId : curriculumByGroupDtoRequest.getDisciplineId()) {
            Curriculum curriculum = new Curriculum();
            if (Objects.nonNull(curriculumByGroupDtoRequest.getEducationProgramId())) educationProgramRepository.findById(curriculumByGroupDtoRequest.getEducationProgramId())
                        .ifPresent(curriculum::setEducationProgram);
            if (Objects.nonNull(curriculumByGroupDtoRequest.getDisciplineId())) disciplineRepository.findById(disciplineId)
                        .ifPresent(curriculum::setDiscipline);
            if (Objects.nonNull(curriculumByGroupDtoRequest.getNumberOfTrimester())) curriculum.setNumberOfTrimester(curriculumByGroupDtoRequest.getNumberOfTrimester());
                curriculum.setGroupedDisciplines(groupedDisciplinesNumber);
            try {
                Curriculum createdCurriculum = curriculumRepository.save(curriculum);
                curriculumHistoryService.createCurriculumHistory(createdCurriculum, userId);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "discipline", TypeOfActionForHistory.CREATE));
            }
        }
    }

    public Long createCurriculum(CurriculumDtoRequest curriculumDtoRequest, Long userId) {
        Curriculum curriculum = new Curriculum();
        if (Objects.nonNull(curriculumDtoRequest.getEducationProgramId())) educationProgramRepository.findById(curriculumDtoRequest.getEducationProgramId())
        .ifPresent(curriculum::setEducationProgram);
        if (Objects.nonNull(curriculumDtoRequest.getDisciplineId())) disciplineRepository.findById(curriculumDtoRequest.getDisciplineId())
                .ifPresent(curriculum::setDiscipline);
        if (Objects.nonNull(curriculumDtoRequest.getNumberOfTrimester())) curriculum.setNumberOfTrimester(curriculumDtoRequest.getNumberOfTrimester());
        curriculum.setGroupedDisciplines(curriculumRepository.findMaxValueOfGroupedDiscipline() + 1);
        try {
            Curriculum createdCurriculum = curriculumRepository.save(curriculum);
            curriculumHistoryService.createCurriculumHistory(createdCurriculum, userId);
            return createdCurriculum.getId();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "discipline", TypeOfActionForHistory.CREATE));
        }
    }

    public void deleteCurriculum(Long curriculumId, Long userId) {
        Curriculum curriculum = curriculumRepository.findById(curriculumId)
                .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Curriculum", "id")));
        try {
            curriculumRepository.deleteById(curriculumId);
            curriculumHistoryService.deleteCurriculumHistory(curriculum, userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "discipline", TypeOfActionForHistory.DELETE));
        }
    }
}
