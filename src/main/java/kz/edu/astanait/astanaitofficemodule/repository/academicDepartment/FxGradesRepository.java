package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.AcademicStream;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.FxGrades;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FxGradesRepository extends JpaRepository<FxGrades, Long> {
}
