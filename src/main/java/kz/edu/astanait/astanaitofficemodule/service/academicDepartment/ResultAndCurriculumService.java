package kz.edu.astanait.astanaitofficemodule.service.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.ResultAndCurriculumDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.ResultAndCurriculumMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.ResultAndCurriculum;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.CurriculumRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.ResultAndCurriculumRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.ResultOfEducationProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ResultAndCurriculumService {

    private final ResultAndCurriculumRepository resultAndCurriculumRepository;
    private final ResultOfEducationProgramRepository resultOfEducationProgramRepository;
    private final CurriculumRepository curriculumRepository;


    @Autowired
    public ResultAndCurriculumService(ResultAndCurriculumRepository resultAndCurriculumRepository, ResultOfEducationProgramRepository resultOfEducationProgramRepository, CurriculumRepository curriculumRepository) {
        this.resultAndCurriculumRepository = resultAndCurriculumRepository;
        this.resultOfEducationProgramRepository = resultOfEducationProgramRepository;
        this.curriculumRepository = curriculumRepository;
    }

    public List<ResultAndCurriculumDtoResponse> getAllResultAndCurriculum(Long educationProgramId) {
        ResultAndCurriculumMapper resultAndCurriculumMapper = new ResultAndCurriculumMapper();
        return resultAndCurriculumRepository.findByResultEducationProgramId(educationProgramId)
                .stream().map(resultAndCurriculumMapper::resultAndCurriculumToDto).collect(Collectors.toList());
    }

    public Long createResultAndCurriculum(Long resultId, Long curriculumId) {
        ResultAndCurriculum resultAndCurriculum = new ResultAndCurriculum();
        resultOfEducationProgramRepository.findById(resultId).ifPresent(resultAndCurriculum::setResult);
        curriculumRepository.findById(curriculumId).ifPresent(resultAndCurriculum::setCurriculum);
        try {
            return resultAndCurriculumRepository.save(resultAndCurriculum).getId();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "result and curriculum", TypeOfActionForHistory.CREATE));
        }
    }

    public void deleteResultAndCurriculum(Long resultAndCurriculumId) {
        try {
            resultAndCurriculumRepository.deleteById(resultAndCurriculumId);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "result and curriculum", TypeOfActionForHistory.DELETE));
        }
    }
}
