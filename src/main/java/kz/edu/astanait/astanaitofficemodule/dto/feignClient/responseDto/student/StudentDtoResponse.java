package kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.CitizenshipDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.FinancingDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.NationalityDtoResponse;
import lombok.Data;


@Data
public class StudentDtoResponse {

    private Long id;

    private String nameKz;

    private String surnameKz;

    private String patronymicKz;

    private String nameEn;

    private String surnameEn;

    private String patronymicEn;

    private NationalityDtoResponse nationality;

    private CitizenshipDtoResponse citizenship;

    private Integer courseGrade;

    private FinancingDtoResponse financing;

    private GroupDtoResponse group;

    private Long educationProgramId;
}
