package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AssessmentReport;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student.StudentDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AcademicStreamDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AcademicStreamStudentGradeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.EducationProgramDtoResponse;
import lombok.Data;

import java.util.List;

@Data
public class SummarySheetForTranscriptDtoResponse {
    private AcademicStreamDtoResponse academicStream;
    private StudentDtoResponse student;
    private EducationProgramDtoResponse educationProgram;
    private List<AcademicStreamStudentGradeDtoResponse> academicStreamStudentGrade;
}

