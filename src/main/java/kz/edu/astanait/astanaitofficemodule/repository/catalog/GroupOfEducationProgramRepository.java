package kz.edu.astanait.astanaitofficemodule.repository.catalog;

import kz.edu.astanait.astanaitofficemodule.model.catalog.GroupOfEducationProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupOfEducationProgramRepository extends JpaRepository<GroupOfEducationProgram, Long> {
}
