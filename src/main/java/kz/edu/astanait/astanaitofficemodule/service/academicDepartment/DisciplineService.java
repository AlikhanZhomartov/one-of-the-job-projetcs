package kz.edu.astanait.astanaitofficemodule.service.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.discipline.DisciplineDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.DisciplineDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.CustomNotFoundException;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.DisciplineMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Discipline;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.AcademicDegreeRepository;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.DepartmentRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.DisciplineRepository;
import kz.edu.astanait.astanaitofficemodule.serviceHistory.academicDepartmentHistory.DisciplineHistoryService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DisciplineService {

    private final DisciplineRepository disciplineRepository;
    private final DepartmentRepository departmentRepository;
    private final DisciplineHistoryService disciplineHistoryService;
    private final AcademicDegreeRepository academicDegreeRepository;

    private static final int pageSize = 30;

    @Autowired
    public DisciplineService(DisciplineRepository disciplineRepository, DepartmentRepository departmentRepository, DisciplineHistoryService disciplineHistoryService, AcademicDegreeRepository academicDegreeRepository) {
        this.disciplineRepository = disciplineRepository;
        this.departmentRepository = departmentRepository;
        this.disciplineHistoryService = disciplineHistoryService;
        this.academicDegreeRepository = academicDegreeRepository;
    }

    private Discipline manipulateDiscipline(DisciplineDtoRequest disciplineDtoRequest, Discipline discipline, Long userId, String action) {
        if (Strings.isNotBlank(disciplineDtoRequest.getTitleEn())) discipline.setTitleEn(disciplineDtoRequest.getTitleEn());
        if (Strings.isNotBlank(disciplineDtoRequest.getTitleRu())) discipline.setTitleRu(disciplineDtoRequest.getTitleRu());
        if (Strings.isNotBlank(disciplineDtoRequest.getTitleKz())) discipline.setTitleKz(disciplineDtoRequest.getTitleKz());
        if (Strings.isNotBlank(disciplineDtoRequest.getDescriptionEn())) discipline.setDescriptionEn(disciplineDtoRequest.getDescriptionEn());
        if (Strings.isNotBlank(disciplineDtoRequest.getDescriptionRu())) discipline.setDescriptionRu(disciplineDtoRequest.getDescriptionRu());
        if (Strings.isNotBlank(disciplineDtoRequest.getDescriptionKz())) discipline.setDescriptionKz(disciplineDtoRequest.getDescriptionKz());
        if (Strings.isNotBlank(disciplineDtoRequest.getCode())) discipline.setCode(disciplineDtoRequest.getCode());
        if (Objects.nonNull(disciplineDtoRequest.getVolumeCredits())) discipline.setVolumeCredits(disciplineDtoRequest.getVolumeCredits());
        if (Objects.nonNull(disciplineDtoRequest.getDepartmentId())) departmentRepository.findById(disciplineDtoRequest.getDepartmentId()).ifPresent(discipline::setDepartment);
        if (Objects.nonNull(disciplineDtoRequest.getAcademicDegreeId())) academicDegreeRepository.findById(disciplineDtoRequest.getAcademicDegreeId()).ifPresent(discipline::setAcademicDegree);
        try {
            return disciplineRepository.save(discipline);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "discipline", action));
        }
    }

    public Long createDiscipline(DisciplineDtoRequest disciplineDtoRequest, Long userId) {
        Discipline discipline = new Discipline();
        Discipline createdDiscipline = manipulateDiscipline(disciplineDtoRequest, discipline, userId, TypeOfActionForHistory.CREATE);
        disciplineHistoryService.createDisciplineHistory(createdDiscipline, userId);
        return createdDiscipline.getId();
    }

    @Transactional
    public void updateDiscipline(DisciplineDtoRequest disciplineDtoRequest, Long disciplineId, Long userId) {
         Discipline discipline = disciplineRepository.findById(disciplineId)
                .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Discipline", "id")));

         disciplineHistoryService.updateDisciplineHistory(discipline, userId);

         manipulateDiscipline(disciplineDtoRequest, discipline, userId, TypeOfActionForHistory.UPDATE);
    }

    public void deleteDiscipline(Long disciplineId, Long userId) {
        Discipline discipline = disciplineRepository.findById(disciplineId)
                .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Discipline", "id")));
        try {
            disciplineRepository.deleteById(discipline.getId());
            disciplineHistoryService.deleteDisciplineHistory(discipline, userId);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "discipline", TypeOfActionForHistory.DELETE));
        }
    }

    public DisciplineDtoResponse getDisciplineByOne(Long id) {
        Discipline discipline = disciplineRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Discipline", "id")));
        return new DisciplineMapper().disciplineToDto(discipline);
    }

    public HashMap<String, Object> getDisciplinesBy30(int page) {
        DisciplineMapper disciplineMapper = new DisciplineMapper();
        PagedListHolder<DisciplineDtoResponse> pagedListHolder = new PagedListHolder<>(disciplineRepository.findAll().
                stream().map(disciplineMapper::disciplineToDto).collect(Collectors.toList()));
        pagedListHolder.setPageSize(pageSize);
        pagedListHolder.setPage(page);
        return pageConvertToMap(pagedListHolder);
    }

    public List<DisciplineDtoResponse> getAllDisciplines() {
        DisciplineMapper disciplineMapper = new DisciplineMapper();
        return disciplineRepository.findAll().stream().map(disciplineMapper::disciplineToDto).collect(Collectors.toList());
    }

    public HashMap<String, Object> getDisciplinesByFilter(Long departmentId, Long academicDegreeId, String keyword, int page) {
        DisciplineMapper disciplineMapper = new DisciplineMapper();
        PagedListHolder<DisciplineDtoResponse> pagedListHolder = new PagedListHolder<>(disciplineRepository.findAllByCustomFilter(departmentId, academicDegreeId, keyword).
                stream().map(disciplineMapper::disciplineToDto).collect(Collectors.toList()));
        pagedListHolder.setPageSize(pageSize);
        pagedListHolder.setPage(page);
        return pageConvertToMap(pagedListHolder);
    }

    public List<DisciplineDtoResponse> getDisciplinesByTeacher(Long teacherId) {
       return disciplineRepository.selectAllByTeacherId(teacherId)
                .stream().map(v -> new DisciplineMapper().disciplineToDto(v)).collect(Collectors.toList());
    }

    private HashMap<String, Object> pageConvertToMap(PagedListHolder<DisciplineDtoResponse> pagedListHolder) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", pagedListHolder.getPageList());
        map.put("number_of_pages", pagedListHolder.getPageCount());
        map.put("current_page", pagedListHolder.getPage());
        map.put("total_number", pagedListHolder.getSource().size());
        return map;
    }

}
