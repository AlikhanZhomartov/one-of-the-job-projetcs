package kz.edu.astanait.astanaitofficemodule.service.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student.GroupDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student.StudentDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.teacher.TeacherDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AssessmentReport.SummarySheetDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AssessmentReport.SummarySheetForTranscriptDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.EducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.feignClient.fileService.FileServiceClient;
import kz.edu.astanait.astanaitofficemodule.feignClient.student.StudentServiceClient;
import kz.edu.astanait.astanaitofficemodule.feignClient.teacher.TeacherServiceClient;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.AcademicStreamMapper;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.AcademicStreamStudentGradeMapper;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.AcademicStreamStudentMapper;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.EducationProgramMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.AcademicStream;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.AcademicStreamStudent;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.EducationProgram;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.AcademicStreamRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.AcademicStreamStudentRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.EducationProgramRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AssessmentReportService {

    private final StudentServiceClient studentServiceClient;
    private final AcademicStreamStudentRepository academicStreamStudentRepository;
    private final TeacherServiceClient teacherServiceClient;
    private final AcademicStreamRepository academicStreamRepository;
    private final EducationProgramRepository educationProgramRepository;
    private final FileServiceClient fileServiceClient;

    @Autowired
    public AssessmentReportService(StudentServiceClient studentServiceClient, AcademicStreamStudentRepository academicStreamStudentRepository, TeacherServiceClient teacherServiceClient, AcademicStreamRepository academicStreamRepository, EducationProgramRepository educationProgramRepository, FileServiceClient fileServiceClient) {
        this.studentServiceClient = studentServiceClient;
        this.academicStreamStudentRepository = academicStreamStudentRepository;
        this.teacherServiceClient = teacherServiceClient;
        this.academicStreamRepository = academicStreamRepository;
        this.educationProgramRepository = educationProgramRepository;
        this.fileServiceClient = fileServiceClient;
    }

    public SummarySheetDtoResponse summarySheetByDisciplineAndGroup(Long disciplineId, Long teacherId, Long groupId, HttpServletRequest request) {
        String token = request.getHeader(AUTHORIZATION);
        List<StudentDtoResponse> studentDtoResponseList;
        TeacherDtoResponse teacherDtoResponse;

        try {
            studentDtoResponseList = studentServiceClient.getStudentsByGroup(token, groupId).getBody();
            teacherDtoResponse = teacherServiceClient.getTeacherById(token, teacherId).getBody();
        }catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "selecting", "summary sheet by discipline and group"));
        }

        if (studentDtoResponseList == null) throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "selecting", "summary sheet by discipline and group"));
        if (teacherDtoResponse == null) throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "selecting", "summary sheet by discipline and group"));

        List<Long> studentsId = new ArrayList<>();
        studentDtoResponseList.forEach(v -> studentsId.add(v.getId()));
        List<AcademicStreamStudent> academicStreamStudentList = academicStreamStudentRepository.findByAcademicStreamStudentGroupAndDiscipline(studentsId, disciplineId, teacherId);

        SummarySheetDtoResponse summarySheetDtoResponse = new SummarySheetDtoResponse();
        summarySheetDtoResponse.setAcademicStream(new AcademicStreamMapper().academicStreamToDto(
                academicStreamStudentList.get(0).getAcademicStream(), teacherDtoResponse));

        academicStreamStudentList = academicStreamStudentList.stream().filter(v ->  !v.getAcademicStreamStudentGrades().isEmpty())
                .collect(Collectors.toList());

        summarySheetDtoResponse.setAcademicStreamStudent(academicStreamStudentList.stream().map(v ->
                new AcademicStreamStudentMapper().academicStreamStudentToDto(v, studentDtoResponseList.stream()
                                .filter(student -> student.getId().equals(v.getStudentId()))
                                .collect(Collectors.toList()).get(0)))
                .collect(Collectors.toList()));

        return summarySheetDtoResponse;

    }

    public List<SummarySheetDtoResponse> summarySheetByTermAndGroup(Long groupId, Integer term, Integer year, HttpServletRequest request) {

        String token = request.getHeader(AUTHORIZATION);
        List<StudentDtoResponse> studentDtoResponseList;

        try {
            studentDtoResponseList = studentServiceClient.getStudentsByGroup(token, groupId).getBody();
        }catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "selecting", "summary sheet by term and group"));
        }

        if (studentDtoResponseList == null || studentDtoResponseList.size() <= 0) throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "selecting", "summary sheet by term and group"));

        List<Long> studentIdList = new ArrayList<>();
        studentDtoResponseList.forEach(v -> studentIdList.add(v.getId()));

        List<AcademicStream> academicStreamList = academicStreamRepository.selectAcademicStreamByGroupAndTermAndYear(groupId, term, year);

        List<SummarySheetDtoResponse> summarySheetDtoResponseList = new ArrayList<>();
        AcademicStreamMapper academicStreamMapper = new AcademicStreamMapper();
        academicStreamList.forEach(v -> {
                SummarySheetDtoResponse summarySheetDtoResponse = new SummarySheetDtoResponse();
                summarySheetDtoResponse.setAcademicStream(academicStreamMapper.academicStreamToDtoWithoutTeacher(v));
                List<AcademicStreamStudent> academicStreamStudentList = v.getAcademicStreamStudent().stream().filter(v2 -> studentIdList.contains(v2.getStudentId())).collect(Collectors.toList());
                summarySheetDtoResponse.setAcademicStreamStudent(academicStreamStudentList.stream().map(v1 ->

                        new AcademicStreamStudentMapper().academicStreamStudentToDto(v1, studentDtoResponseList.stream()
                                .filter(student -> student.getId().equals(v1.getStudentId()))
                                .collect(Collectors.toList()).get(0)))
                        .collect(Collectors.toList()));
                summarySheetDtoResponseList.add(summarySheetDtoResponse);
        });

        return summarySheetDtoResponseList;
    }

    public HashMap<Integer, List<SummarySheetForTranscriptDtoResponse>> summarySheetForTranscript(Long userId, HttpServletRequest request) {

        String authorization = request.getHeader(AUTHORIZATION);
        StudentDtoResponse studentDtoResponse = studentServiceClient.getStudentsByUserId(authorization, userId).getBody();


        if (studentDtoResponse == null) throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "selecting", "summary sheet for transcript"));

        EducationProgram educationProgram = educationProgramRepository.findById(studentDtoResponse.getEducationProgramId())
                .orElseThrow(()-> new RepositoryException(String.format(ExceptionDescription.RepositoryException, "selecting", "summary sheet for transcript")));

        List<AcademicStreamStudent> academicStreamStudentList =  academicStreamStudentRepository.findAllByStudentId(studentDtoResponse.getId());
        List<SummarySheetForTranscriptDtoResponse> summarySheetForTranscriptDtoResponseList = new ArrayList<>();
        AcademicStreamMapper academicStreamMapper = new AcademicStreamMapper();

        academicStreamStudentList.forEach(v -> {
            SummarySheetForTranscriptDtoResponse summarySheetForTranscriptDtoResponse = new SummarySheetForTranscriptDtoResponse();
            summarySheetForTranscriptDtoResponse.setAcademicStream(academicStreamMapper.academicStreamToDtoWithoutTeacher(v.getAcademicStream()));
            summarySheetForTranscriptDtoResponse.setEducationProgram(new EducationProgramMapper().educationProgramToDto(educationProgram));
            summarySheetForTranscriptDtoResponse.setStudent(studentDtoResponse);
            summarySheetForTranscriptDtoResponse.setAcademicStreamStudentGrade(v.getAcademicStreamStudentGrades().stream().map(AcademicStreamStudentGradeMapper::academicStreamStudentGradeToDto)
            .collect(Collectors.toList()));
            summarySheetForTranscriptDtoResponseList.add(summarySheetForTranscriptDtoResponse);
        });

        List<Integer> numberOfYears = new ArrayList<>();
        for (SummarySheetForTranscriptDtoResponse summarySheetForTranscriptDtoResponse : summarySheetForTranscriptDtoResponseList) {
            boolean flag = false;
            if (!numberOfYears.isEmpty()) {
                for (Integer numberOfYear : numberOfYears) {
                    if (numberOfYear.equals(summarySheetForTranscriptDtoResponse.getAcademicStream().getYear())) {
                        flag = true;
                        break;
                    }
                }
            }
            if (!flag) {
                numberOfYears.add(summarySheetForTranscriptDtoResponse.getAcademicStream().getYear());
            }
        }

        if (numberOfYears.isEmpty()) throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "selecting", "summary sheet for transcript"));

        LocalDate educationStartedYear = educationProgram.getEducationStartedDate();

        HashMap<Integer, List<SummarySheetForTranscriptDtoResponse>> map = new HashMap<>();
        Collections.sort(numberOfYears);

        for (int i = 0; i < numberOfYears.size(); i++) {
            int termCount = 0;
            while (termCount <= 3) {
                termCount++;
                Integer finalTermCount = termCount;
                int finalI = i;
                List<SummarySheetForTranscriptDtoResponse> summarySheetForTranscriptDtoResponseList1 = summarySheetForTranscriptDtoResponseList.
                        stream().filter(year -> year.getAcademicStream().getYear().equals(numberOfYears.get(finalI)) && year.getAcademicStream().getTerm().equals(finalTermCount) && !year.getAcademicStreamStudentGrade().isEmpty())
                        .collect(Collectors.toList());
                int sum = numberOfYears.get(i) - educationStartedYear.getYear();
                if (!summarySheetForTranscriptDtoResponseList1.isEmpty()) {
                    if (sum == 0) {
                        map.put(termCount, summarySheetForTranscriptDtoResponseList1);
                    }
                    else if(sum == 1) {
                        map.put(termCount + 3, summarySheetForTranscriptDtoResponseList1);
                    }
                    else if(sum == 2) {
                        map.put(termCount + 6, summarySheetForTranscriptDtoResponseList1);
                    }
                }
            }
        }
        map.put(0, summarySheetForTranscriptDtoResponseList.stream().filter(v -> v.getAcademicStream().getTerm() == 0 && !v.getAcademicStreamStudentGrade().isEmpty()).collect(Collectors.toList()));
        return map;
    }

    public byte[] summarySheetForTranscriptInPDF(Long userId, HttpServletRequest request) {
        HashMap<Integer, List<SummarySheetForTranscriptDtoResponse>> map =  summarySheetForTranscript(userId, request);
        return fileServiceClient.getTranscript(map).getBody();
    }

    public byte[] summarySheetByDisciplineAndGroupInExcel(Long disciplineId, Long teacherId, Long groupId, HttpServletRequest request) {
        SummarySheetDtoResponse summarySheetDtoResponse = summarySheetByDisciplineAndGroup(disciplineId, teacherId, groupId, request);
        return fileServiceClient.getSummaryBill(summarySheetDtoResponse).getBody();
    }

    public byte[] summarySheetByTermAndGroupInExcel(Long groupId, Integer term, Integer year, HttpServletRequest request) {
        List<SummarySheetDtoResponse> summarySheetDtoResponse = summarySheetByTermAndGroup(groupId, term, year, request);
        return fileServiceClient.getSemesterBill(summarySheetDtoResponse).getBody();
    }

    public void allSummarySheetByTermAndGroupInExcel(Integer term, Integer year, HttpServletRequest request) {
        String authorization = request.getHeader(AUTHORIZATION);
        List<GroupDtoResponse> groupDtoResponses = studentServiceClient.getAllGroup(authorization).getBody();
        groupDtoResponses.forEach(v -> {
            Path path = Paths.get("C:\\Users\\Урал\\Excel\\" + v.getTitle() + ".xls");
            try {
                Files.write(path, summarySheetByTermAndGroupInExcel(v.getId(), term, year, request));
            } catch (IOException e) {
                System.out.println(v.getId() + " " + v.getTitle());
                e.printStackTrace();
            }
        });
    }

}
