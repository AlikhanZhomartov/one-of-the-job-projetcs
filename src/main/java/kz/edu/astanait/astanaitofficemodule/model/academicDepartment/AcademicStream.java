package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.catalog.LessonsType;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@Table(name = "academic_stream")
public class AcademicStream {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "teacher_id")
    private Long teacherId;

    @ManyToOne
    @JoinColumn(name = "discipline_id")
    private Discipline discipline;

    @Column(name = "year")
    private Integer year;

    @Column(name = "senior")
    private Boolean senior;

    @Column(name = "stream_started_date")
    private LocalDate streamStartedDate;

    @Column(name = "stream_ended_date")
    private LocalDate streamEndedDate;

    @Column(name = "term")
    private Integer term;

    @Column(name = "stream_name")
    private String streamName;

    @OneToMany(mappedBy = "academicStream")
    private List<AcademicStreamStudent> academicStreamStudent;

    @ManyToOne
    @JoinColumn(name = "stream_type_id")
    private LessonsType streamType;
}
