package kz.edu.astanait.astanaitofficemodule.mapper.catalog;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.GroupOfEducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.catalog.GroupOfEducationProgram;
import org.apache.logging.log4j.util.Strings;

import java.util.Objects;

public class GroupOfEducationProgramMapper {

    public GroupOfEducationProgramDtoResponse groupOfEducationProgramToDto(GroupOfEducationProgram groupOfEducationProgram) {
        GroupOfEducationProgramDtoResponse groupOfEducationProgramDtoResponse = new GroupOfEducationProgramDtoResponse();
        groupOfEducationProgramDtoResponse.setId(groupOfEducationProgram.getId());
        if (Strings.isNotBlank(groupOfEducationProgram.getTitleEn())) groupOfEducationProgramDtoResponse.setTitleEn(groupOfEducationProgram.getTitleEn());
        if (Strings.isNotBlank(groupOfEducationProgram.getTitleRu())) groupOfEducationProgramDtoResponse.setTitleRu(groupOfEducationProgram.getTitleRu());
        if (Strings.isNotBlank(groupOfEducationProgram.getTitleKz())) groupOfEducationProgramDtoResponse.setTitleKz(groupOfEducationProgram.getTitleKz());
        if (Strings.isNotBlank(groupOfEducationProgram.getCode())) groupOfEducationProgramDtoResponse.setCode(groupOfEducationProgram.getCode());
        if (Objects.nonNull(groupOfEducationProgram.getParent())) groupOfEducationProgramDtoResponse.setParent(groupOfEducationProgram.getParent());
        return groupOfEducationProgramDtoResponse;
    }
}
