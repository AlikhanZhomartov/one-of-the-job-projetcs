package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.ResultOfEducationProgram;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultOfEducationProgramRepository extends JpaRepository<ResultOfEducationProgram, Long> {
    List<ResultOfEducationProgram> findByEducationProgramId(Long id);
    void deleteByEducationProgramId(Long id);
}
