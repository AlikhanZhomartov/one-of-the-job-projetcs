package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.catalog.AcademicDegree;
import kz.edu.astanait.astanaitofficemodule.model.catalog.Department;
import kz.edu.astanait.astanaitofficemodule.model.catalog.EducationLanguage;
import kz.edu.astanait.astanaitofficemodule.model.catalog.GroupOfEducationProgram;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Entity
@Data
@Table(name = "education_program")
public class EducationProgram {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title_en")
    private String titleEn;

    @Column(name = "title_ru")
    private String titleRu;

    @Column(name = "title_kz")
    private String titleKz;

    @Column(name = "description_en")
    private String descriptionEn;

    @Column(name = "description_ru")
    private String descriptionRu;

    @Column(name = "description_kz")
    private String descriptionKz;

    @Column(name = "distinctive_features_en")
    private String distinctiveFeaturesEn;

    @Column(name = "distinctive_features_ru")
    private String distinctiveFeaturesRu;

    @Column(name = "distinctive_features_kz")
    private String distinctiveFeaturesKz;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @Column(name = "type_of_education_program")
    private String typeOfEducationProgram;

    @Column(name = "type_of_response")
    private String typeOfResponse;

    @Column(name = "status")
    private String status;

    @Column(name = "level_by_NRK")
    private String levelByNRK;

    @Column(name = "level_by_ORK")
    private String levelByORK;

    @ManyToOne
    @JoinColumn(name = "academic_degree_id")
    private AcademicDegree academicDegree;

    @Column(name = "education_started_date")
    private LocalDate educationStartedDate;

    @Column(name = "volume_credits")
    private Integer volumeCredits;

    @ManyToOne
    @JoinColumn(name = "education_language_id")
    private EducationLanguage educationLanguage;

    @ManyToOne
    @JoinColumn(name = "group_of_education_program_id")
    private GroupOfEducationProgram groupOfEducationProgram;

    @Column(name = "approval_date_by_academic_council")
    private LocalDate approvalDateByAcademicCouncil;

    @Column(name = "license")
    private Boolean license;

    @Column(name = "accreditation")
    private Boolean accreditation;
}
