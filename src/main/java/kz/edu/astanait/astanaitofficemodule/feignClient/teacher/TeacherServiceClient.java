package kz.edu.astanait.astanaitofficemodule.feignClient.teacher;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.teacher.TeacherDtoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "astanait-teacher-module")
public interface TeacherServiceClient {

    @GetMapping("/api/v1/teacher/get-teacher-by-id")
    ResponseEntity<TeacherDtoResponse> getTeacherById(@RequestHeader("Authorization") String token, @RequestParam(name = "teacher_id") Long teacherId);
}
