package kz.edu.astanait.astanaitofficemodule.mapper.catalog;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.AcademicDegreeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.catalog.AcademicDegree;
import org.apache.logging.log4j.util.Strings;

public class AcademicDegreeMapper {
    public AcademicDegreeDtoResponse academicDegreeToDto(AcademicDegree academicDegree) {
        AcademicDegreeDtoResponse academicDegreeDtoResponse = new AcademicDegreeDtoResponse();
        academicDegreeDtoResponse.setId(academicDegree.getId());
        if (Strings.isNotBlank(academicDegree.getTitleEn())) academicDegreeDtoResponse.setTitleEn(academicDegree.getTitleEn());
        if (Strings.isNotBlank(academicDegree.getTitleRu())) academicDegreeDtoResponse.setTitleRu(academicDegree.getTitleRu());
        if (Strings.isNotBlank(academicDegree.getTitleKz())) academicDegreeDtoResponse.setTitleKz(academicDegree.getTitleKz());
        return academicDegreeDtoResponse;
    }
}
