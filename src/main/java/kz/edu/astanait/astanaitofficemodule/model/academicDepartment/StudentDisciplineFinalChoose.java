package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "student_discipline_final_choose")
public class StudentDisciplineFinalChoose {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "curriculum_id")
    private Curriculum curriculumId;

    @Column(name = "student_id")
    private Long studentId;
}
