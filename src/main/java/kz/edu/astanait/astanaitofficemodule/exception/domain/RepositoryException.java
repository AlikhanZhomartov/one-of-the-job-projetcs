package kz.edu.astanait.astanaitofficemodule.exception.domain;

public class RepositoryException extends RuntimeException {
    public RepositoryException(String message) {
        super(message);
    }
}
