package kz.edu.astanait.astanaitofficemodule.exception.domain;

public class CustomNotFoundException extends RuntimeException {
    public CustomNotFoundException(String message) {
        super(message);
    }
}