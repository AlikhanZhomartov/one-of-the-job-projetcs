package kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.discipline;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DisciplineDtoRequest {

    @NotBlank(message = "Название на английском не может быть пустым!")
    private String titleEn;

    @NotBlank(message = "Название на русском не может быть пустым!")
    private String titleRu;

    @NotBlank(message = "Название на казахском не может быть пустым!")
    private String titleKz;

    @NotBlank(message = "Описание на английском не может быть пустым!")
    private String descriptionEn;

    @NotBlank(message = "Описание на русском не может быть пустым!")
    private String descriptionRu;

    @NotBlank(message = "Описание на казахском не может быть пустым!")
    private String descriptionKz;

    @NotBlank(message = "Код предмета не должен быть пустым!")
    private String code;

    @NotNull(message = "Количество кредитов не должно быть нулевым!")
    private Integer volumeCredits;

    @NotNull(message = "Департамент должен быть выбран!")
    private Long departmentId;

    @NotNull(message = "Академическая степень должна быть выбрана!")
    private Long academicDegreeId;
}
