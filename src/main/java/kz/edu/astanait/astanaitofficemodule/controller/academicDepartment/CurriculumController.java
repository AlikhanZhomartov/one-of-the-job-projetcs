package kz.edu.astanait.astanaitofficemodule.controller.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.Curriculum.CurriculumByGroupDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.Curriculum.CurriculumDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.CurriculumDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionHandling;
import kz.edu.astanait.astanaitofficemodule.service.academicDepartment.CurriculumService;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/v1/academic-department/curriculum")
public class CurriculumController extends ExceptionHandling {

    private final CurriculumService curriculumService;

    @Autowired
    public CurriculumController(CurriculumService curriculumService) {
        this.curriculumService = curriculumService;
    }

    @PreAuthorize("hasAuthority('get-all-curriculums')")
    @GetMapping("/get-all-curriculums")
    public ResponseEntity<List<CurriculumDtoResponse>> getAllCurriculums() {
        return new ResponseEntity<>(curriculumService.getAllCurriculums(), OK);
    }

    @PreAuthorize("hasAuthority('get-curriculums-by-education-program')")
    @GetMapping("/get-curriculums-by-education-program")
    public ResponseEntity<List<CurriculumDtoResponse>> getCurriculumsByEducationProgramId(@RequestParam(name = "education_program_id") Long educationProgramId) {
        return new ResponseEntity<>(curriculumService.getCurriculumsByEducationProgram(educationProgramId), OK);
    }

    @PreAuthorize("hasAuthority('create-curriculum')")
    @PostMapping("/create-curriculum")
    public ResponseEntity<Long> createCurriculum(@Valid CurriculumDtoRequest curriculumDtoRequest, Principal principal) {
        return new ResponseEntity<>(curriculumService.createCurriculum(curriculumDtoRequest, Long.parseLong(principal.getName())), OK);
    }

    @PreAuthorize("hasAuthority('create-curriculum-by-group')")
    @PostMapping("/create-curriculum-by-group")
    public ResponseEntity<HttpStatus> createCurriculumByGroup(@Valid CurriculumByGroupDtoRequest curriculumByGroupDtoRequest, Principal principal) {
        curriculumService.createCurriculumByGroup(curriculumByGroupDtoRequest, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasAuthority('delete-curriculum')")
    @DeleteMapping("/delete-curriculum")
    public ResponseEntity<HttpStatus> deleteCurriculum(@RequestParam(name = "curriculum_id") Long curriculumId, Principal principal) {
        curriculumService.deleteCurriculum(curriculumId, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }
}
