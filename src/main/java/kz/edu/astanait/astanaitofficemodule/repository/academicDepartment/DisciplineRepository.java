package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Discipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DisciplineRepository extends JpaRepository<Discipline, Long> {
    @Query(value = "select * from discipline d inner join department d2 on d2.id = d.department_id\n" +
            "inner join academic_degree ad on ad.id = d.academic_degree_id\n" +
            "where case when ?1 != 0 then d.id = ?1 else true end and case when ?2 != 0" +
            " then ad.id = ?2 else true end and case when ?3 is not null then (d.title_en like '%?3%'" +
            " or d.title_ru like '%?3%' or d.title_kz like '%?3%') else true end;", nativeQuery = true)
    List<Discipline> findAllByCustomFilter(Long departmentId, Long academicDegreeId, String title);

    @Query(nativeQuery = true, value = "select d.id, title_en, title_ru, title_kz, description_en, description_ru, description_kz, code, volume_credits, department_id, academic_degree_id from discipline d inner join academic_stream \"as\" on d.id = \"as\".discipline_id\n" +
            "where \"as\".teacher_id = ?1;")
    List<Discipline> selectAllByTeacherId(Long teacherId);
}
