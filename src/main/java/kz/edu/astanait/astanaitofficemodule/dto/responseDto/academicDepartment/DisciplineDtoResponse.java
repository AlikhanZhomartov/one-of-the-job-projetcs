package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.AcademicDegreeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.DepartmentDtoResponse;
import lombok.Data;

@Data
public class DisciplineDtoResponse {
    private Long id;

    private String titleEn;

    private String titleRu;

    private String titleKz;

    private String descriptionEn;

    private String descriptionRu;

    private String descriptionKz;

    private String code;

    private Integer volumeCredits;

    private DepartmentDtoResponse department;

    private AcademicDegreeDtoResponse academicDegree;
}
