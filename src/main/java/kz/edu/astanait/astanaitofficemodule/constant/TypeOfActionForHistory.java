package kz.edu.astanait.astanaitofficemodule.constant;

public class TypeOfActionForHistory {
    public static final String CREATE = "creating";
    public static final String UPDATE = "updating";
    public static final String DELETE = "deleting";
}
