package kz.edu.astanait.astanaitofficemodule.feignClient.auth;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.auth.JWTDtoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "astanait-authorization-module")
public interface AuthorizationServiceClient {

    @GetMapping("/api/v1/jwt/jwt-token-decryption")
    ResponseEntity<JWTDtoResponse> jwtTokenDecryption(@RequestHeader("Authorization") String token);
}

