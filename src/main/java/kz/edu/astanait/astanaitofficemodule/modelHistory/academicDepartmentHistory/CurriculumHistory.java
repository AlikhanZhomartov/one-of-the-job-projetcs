package kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Discipline;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.EducationProgram;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "z_curriculum_history")
public class CurriculumHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "curriculum_id")
    private Long curriculumId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "education_program_id")
    private Long educationProgramId;

    @Column(name = "discipline_id")
    private Long disciplineId;

    @Column(name = "number_of_trimester")
    private Integer numberOfTrimester;

    @Column(name = "grouped_disciplines")
    private Long groupedDisciplines;

    @Column(name = "type_of_action")
    private String typeOfAction;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }

}
