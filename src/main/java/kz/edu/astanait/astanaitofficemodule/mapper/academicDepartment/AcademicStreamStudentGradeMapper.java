package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AcademicStreamStudentGradeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.AcademicStreamStudentGrade;

import java.util.Objects;

public class AcademicStreamStudentGradeMapper {

    public static AcademicStreamStudentGradeDtoResponse academicStreamStudentGradeToDto (AcademicStreamStudentGrade academicStreamStudentGrade) {
        AcademicStreamStudentGradeDtoResponse academicStreamStudentGradeDtoResponse = new AcademicStreamStudentGradeDtoResponse();
        if (Objects.nonNull(academicStreamStudentGrade.getGrade())) academicStreamStudentGradeDtoResponse.setGrade(academicStreamStudentGrade.getGrade());
        if (Objects.nonNull(academicStreamStudentGrade.getGradeType())) academicStreamStudentGradeDtoResponse.setGradeType(academicStreamStudentGrade.getGradeType());
        if (Objects.nonNull(academicStreamStudentGrade.getApproved())) academicStreamStudentGradeDtoResponse.setApproved(academicStreamStudentGrade.getApproved());
        if (Objects.nonNull(academicStreamStudentGrade.getCreatedDate())) academicStreamStudentGradeDtoResponse.setCreatedDate(academicStreamStudentGrade.getCreatedDate());
        return academicStreamStudentGradeDtoResponse;
    }
}
