package kz.edu.astanait.astanaitofficemodule.serviceHistory.academicDepartmentHistory;

import static kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory.*;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.BreakDownByHour;
import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.BreakDownByHourHistory;
import kz.edu.astanait.astanaitofficemodule.repositoryHistory.academicDepartmentHistory.BreakDownByHourHistoryRepository;
import org.springframework.stereotype.Service;


@Service
public class BreakDownByHourHistoryService {

    private final BreakDownByHourHistoryRepository breakDownByHourHistoryRepository;

    public BreakDownByHourHistoryService(BreakDownByHourHistoryRepository breakDownByHourHistoryRepository) {
        this.breakDownByHourHistoryRepository = breakDownByHourHistoryRepository;
    }

    public void createBreakDownByHourHistory(BreakDownByHour breakDownByHour, Long userId) {
        manipulateBreakDownByHourHistory(breakDownByHour, userId, CREATE);
    }

    public void updateBreakDownByHourHistory(BreakDownByHour breakDownByHour, Long userId) {
        manipulateBreakDownByHourHistory(breakDownByHour, userId, UPDATE);
    }

    public void deleteBreakDownByHourHistory(Long disciplineId, Long userId) {
        BreakDownByHourHistory breakDownByHourHistory = new BreakDownByHourHistory();
        breakDownByHourHistory.setDisciplineId(disciplineId);
        breakDownByHourHistory.setUserId(userId);
        breakDownByHourHistory.setTypeOfAction(DELETE);
        try {
            breakDownByHourHistoryRepository.save(breakDownByHourHistory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void manipulateBreakDownByHourHistory(BreakDownByHour breakDownByHour, Long userId, String typeOfAction) {
        BreakDownByHourHistory breakDownByHourHistory = new BreakDownByHourHistory();
        breakDownByHourHistory.setBreakDownByHourId(breakDownByHour.getId());
        breakDownByHourHistory.setUserId(userId);
        breakDownByHourHistory.setDisciplineId(breakDownByHour.getDiscipline().getId());
        breakDownByHourHistory.setLessonsTypeId(breakDownByHour.getLessonsType().getId());
        breakDownByHourHistory.setHours(breakDownByHour.getHours());
        breakDownByHourHistory.setTypeOfAction(typeOfAction);
        try {
            breakDownByHourHistoryRepository.save(breakDownByHourHistory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
