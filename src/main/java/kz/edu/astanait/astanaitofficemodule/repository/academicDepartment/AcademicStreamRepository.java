package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.AcademicStream;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AcademicStreamRepository extends JpaRepository<AcademicStream, Long> {
    @Query(nativeQuery = true, value = "select academic_stream.id, teacher_id, senior, year, stream_type_id, stream_started_date, stream_ended_date, term, discipline_id, stream_name from academic_stream inner join academic_stream_students ass on academic_stream.id = ass.academic_stream_id inner join students s on s.id = ass.student_id\n" +
            "inner join academic_stream_students_grades assg on ass.id = assg.academic_stream_student_id\n" +
            "where s.group_id = ?1 and academic_stream.term = ?2 and academic_stream.year = ?3 and assg IS NOT NULL group by academic_stream.id;")
    List<AcademicStream> selectAcademicStreamByGroupAndTermAndYear(Long groupId, Integer term, Integer year);
}
