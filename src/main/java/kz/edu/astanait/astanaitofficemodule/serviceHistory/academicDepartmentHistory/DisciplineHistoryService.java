package kz.edu.astanait.astanaitofficemodule.serviceHistory.academicDepartmentHistory;


import static kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory.*;

import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Discipline;
import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.DisciplineHistory;
import kz.edu.astanait.astanaitofficemodule.repositoryHistory.academicDepartmentHistory.DisciplineHistoryRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;


@Service
public class DisciplineHistoryService {

    private final DisciplineHistoryRepository disciplineHistoryRepository;

    @Autowired
    public DisciplineHistoryService(DisciplineHistoryRepository disciplineHistoryRepository) {
        this.disciplineHistoryRepository = disciplineHistoryRepository;
    }

    public void createDisciplineHistory(Discipline discipline, Long userId) {
        manipulateDisciplineHistory(discipline, userId, CREATE);
    }

    public void updateDisciplineHistory(Discipline discipline, Long userId) {
        manipulateDisciplineHistory(discipline, userId, UPDATE);
    }

    public void deleteDisciplineHistory(Discipline discipline, Long userId) {
        manipulateDisciplineHistory(discipline, userId, DELETE);
    }

    private void manipulateDisciplineHistory(Discipline discipline, Long userId, String typeOfAction) {
        DisciplineHistory disciplineHistory = new DisciplineHistory();
        disciplineHistory.setDisciplineId(discipline.getId());
        disciplineHistory.setUser_id(userId);
        disciplineHistory.setTypeOfAction(typeOfAction);
        if (Strings.isNotBlank(discipline.getTitleEn())) disciplineHistory.setTitleEn(discipline.getTitleEn());
        if (Strings.isNotBlank(discipline.getTitleRu())) disciplineHistory.setTitleRu(discipline.getTitleRu());
        if (Strings.isNotBlank(discipline.getTitleKz())) disciplineHistory.setTitleKz(discipline.getTitleKz());
        if (Strings.isNotBlank(discipline.getDescriptionEn())) disciplineHistory.setDescriptionEn(discipline.getDescriptionEn());
        if (Strings.isNotBlank(discipline.getDescriptionRu())) disciplineHistory.setDescriptionRu(discipline.getDescriptionRu());
        if (Strings.isNotBlank(discipline.getDescriptionKz())) disciplineHistory.setDescriptionKz(discipline.getDescriptionKz());
        if (Strings.isNotBlank(discipline.getCode())) disciplineHistory.setCode(discipline.getCode());
        if (Objects.nonNull(discipline.getVolumeCredits())) disciplineHistory.setVolumeCredits(discipline.getVolumeCredits());
        if (Objects.nonNull(discipline.getDepartment())) disciplineHistory.setDepartmentId(discipline.getDepartment().getId());
        if (Objects.nonNull(discipline.getAcademicDegree())) disciplineHistory.setAcademicDegreeId(discipline.getAcademicDegree().getId());
        try {
            disciplineHistoryRepository.save(disciplineHistory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
