package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.BreakDownByHour;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BreakDownByHourRepository extends JpaRepository<BreakDownByHour, Long> {
    List<BreakDownByHour> findAllByDisciplineId(Long id);
    void deleteByDisciplineId(Long disciplineId);
}
