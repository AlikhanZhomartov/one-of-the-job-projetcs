package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import lombok.Data;

@Data
public class PreviousCurriculumDisciplineDtoResponse {
    private Long id;

    private CurriculumDtoResponse curriculum;

    private CurriculumDtoResponse previousCurriculumDiscipline;
}
