package kz.edu.astanait.astanaitofficemodule.controller.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.PreviousCurriculumDisciplineDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionHandling;
import kz.edu.astanait.astanaitofficemodule.service.academicDepartment.PreviousCurriculumDisciplineService;
import static org.springframework.http.HttpStatus.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/academic-department/previous-curriculum-discipline")
public class PreviousCurriculumDisciplineController extends ExceptionHandling {

    private final PreviousCurriculumDisciplineService previousCurriculumDisciplineService;

    public PreviousCurriculumDisciplineController(PreviousCurriculumDisciplineService previousCurriculumDisciplineService) {
        this.previousCurriculumDisciplineService = previousCurriculumDisciplineService;
    }

    @PreAuthorize("hasAuthority('get-previous-curriculum-discipline')")
    @GetMapping("/get-previous-curriculum-discipline")
    public ResponseEntity<List<PreviousCurriculumDisciplineDtoResponse>> getPreviousCurriculumDisciplineByCurriculum(@RequestParam(name = "curriculum_id") Long curriculumId) {
        return new ResponseEntity<>(previousCurriculumDisciplineService.getPreviousCurriculumDisciplineByCurriculum(curriculumId), OK);
    }

    @PreAuthorize("hasAuthority('create-previous-curriculum-discipline')")
    @PostMapping("/create-previous-curriculum-discipline")
    public ResponseEntity<Long> createPreviousCurriculumDiscipline(@RequestParam(name = "curriculum_id") Long curriculumId,
                                                                   @RequestParam(name = "previous_curriculum_id") Long previousCurriculumId) {
        return new ResponseEntity<>(previousCurriculumDisciplineService.createPreviousCurriculumDiscipline(curriculumId, previousCurriculumId), OK);
    }

    @PreAuthorize("hasAuthority('delete-previous-curriculum-discipline')")
    @DeleteMapping("/delete-previous-curriculum-discipline")
    public ResponseEntity<HttpStatus> deletePreviousCurriculumDiscipline(@RequestParam(name = "curriculum_id") Long curriculumId) {
        previousCurriculumDisciplineService.deletePreviousCurriculumDiscipline(curriculumId);
        return new ResponseEntity<>(OK);
    }

}
