package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.LessonsTypeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.catalog.LessonsType;
import lombok.Data;


@Data
public class BreakDownByHourDtoResponse {
    private Long id;

    private Long disciplineId;

    private LessonsTypeDtoResponse lessonsTypeId;

    private Integer hours;
}
