package kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@Table(name = "z_education_program_history")
public class EducationProgramHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "education_program_id")
    private Long educationProgramId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "title_en")
    private String titleEn;

    @Column(name = "title_ru")
    private String titleRu;

    @Column(name = "title_kz")
    private String titleKz;

    @Column(name = "description_en")
    private String descriptionEn;

    @Column(name = "description_ru")
    private String descriptionRu;

    @Column(name = "description_kz")
    private String descriptionKz;

    @Column(name = "distinctive_features_en")
    private String distinctiveFeaturesEn;

    @Column(name = "distinctive_features_ru")
    private String distinctiveFeaturesRu;

    @Column(name = "distinctive_features_kz")
    private String distinctiveFeaturesKz;

    @Column(name = "department_id")
    private Long departmentId;

    @Column(name = "type_of_education_program")
    private String typeOfEducationProgram;

    @Column(name = "type_of_response")
    private String typeOfResponse;

    @Column(name = "status")
    private String status;

    @Column(name = "level_by_NRK")
    private String levelByNRK;

    @Column(name = "level_by_ORK")
    private String levelByORK;

    @Column(name = "academic_degree_id")
    private Long academicDegreeId;

    @Column(name = "education_started_date")
    private LocalDate educationStartedDate;

    @Column(name = "volume_credits")
    private Integer volumeCredits;

    @Column(name = "education_language_id")
    private Long educationLanguageId;

    @Column(name = "group_of_education_program_id")
    private Long groupOfEducationProgramId;

    @Column(name = "approval_date_by_academic_council")
    private LocalDate approvalDateByAcademicCouncil;

    @Column(name = "license")
    private Boolean license;

    @Column(name = "accreditation")
    private Boolean accreditation;

    @Column(name = "type_of_action")
    private String typeOfAction;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }
}
