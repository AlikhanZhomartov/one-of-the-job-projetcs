package kz.edu.astanait.astanaitofficemodule.model.catalog;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "education_language")
public class EducationLanguage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title_en")
    private String titleEn;

    @Column(name = "title_ru")
    private String titleRu;

    @Column(name = "title_kz")
    private String titleKz;
}
