package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.StudentDisciplineChoose;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDisciplineChooseRepository extends JpaRepository<StudentDisciplineChoose, Long> {
}
