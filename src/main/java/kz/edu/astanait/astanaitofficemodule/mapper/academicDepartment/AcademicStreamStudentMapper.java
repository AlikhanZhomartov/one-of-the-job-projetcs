package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student.StudentDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AcademicStreamStudentDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AcademicStreamStudentGradeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.AcademicStreamStudent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AcademicStreamStudentMapper {

    public AcademicStreamStudentDtoResponse academicStreamStudentToDto(AcademicStreamStudent academicStreamStudent, StudentDtoResponse studentDtoResponse) {
        AcademicStreamStudentDtoResponse academicStreamStudentDtoResponse = new AcademicStreamStudentDtoResponse();
        academicStreamStudentDtoResponse.setStudent(studentDtoResponse);
        if (Objects.nonNull(academicStreamStudent.getAcademicStreamStudentGrades())) {
            List<AcademicStreamStudentGradeDtoResponse> academicStreamStudentGradeDtoResponseList = new ArrayList<>();
            academicStreamStudent.getAcademicStreamStudentGrades().forEach(
                    v -> academicStreamStudentGradeDtoResponseList.add(AcademicStreamStudentGradeMapper.academicStreamStudentGradeToDto(v))
            );
            academicStreamStudentDtoResponse.setAcademicStreamStudentGrade(academicStreamStudentGradeDtoResponseList);
        }
        return academicStreamStudentDtoResponse;
    }
}
