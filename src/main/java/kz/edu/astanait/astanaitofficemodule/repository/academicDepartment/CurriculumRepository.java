package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Curriculum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CurriculumRepository extends JpaRepository<Curriculum, Long> {
    @Query(value = "select max(grouped_disciplines) from curriculum", nativeQuery = true)
    Long findMaxValueOfGroupedDiscipline();
    List<Curriculum> findByEducationProgramId(Long id);
}
