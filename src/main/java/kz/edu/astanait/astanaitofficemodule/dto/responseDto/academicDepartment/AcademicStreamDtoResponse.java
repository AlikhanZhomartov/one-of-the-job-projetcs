package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.teacher.TeacherDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.LessonsTypeDtoResponse;
import lombok.Data;

import java.time.LocalDate;

@Data
public class AcademicStreamDtoResponse {

    private TeacherDtoResponse teacher;

    private DisciplineDtoResponse discipline;

    private Integer year;

    private Boolean senior;

    private LocalDate streamStartedDate;

    private LocalDate streamEndedDate;

    private Integer term;

    private String streamName;

    private LessonsTypeDtoResponse streamType;

}
