package kz.edu.astanait.astanaitofficemodule.service;


import kz.edu.astanait.astanaitofficemodule.model.Office;
import kz.edu.astanait.astanaitofficemodule.repository.OfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    private final OfficeRepository officeRepository;

    @Autowired
    public AuthService(OfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
    }

    public void registerStudent(Long userId) {
        Office createdOffice = new Office();
        createdOffice.setUserId(userId);
        try {
            officeRepository.save(createdOffice);
        } catch (Exception e){e.printStackTrace();}
    }

}
