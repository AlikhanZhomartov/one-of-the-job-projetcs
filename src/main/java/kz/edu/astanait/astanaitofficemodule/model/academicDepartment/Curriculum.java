package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "curriculum")
public class Curriculum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "education_program_id")
    private EducationProgram educationProgram;

    @ManyToOne
    @JoinColumn(name = "discipline_id")
    private Discipline discipline;

    @Column(name = "number_of_trimester")
    private Integer numberOfTrimester;

    @Column(name = "grouped_disciplines")
    private Long groupedDisciplines;
}
