package kz.edu.astanait.astanaitofficemodule.controller.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.breakDownByHour.CreateBreakDownByHourDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.breakDownByHour.UpdateBreakDownByHourDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.BreakDownByHourDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionHandling;
import kz.edu.astanait.astanaitofficemodule.service.academicDepartment.BreakDownByHourService;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/v1/academic-department/break-down-by-hour")
public class BreakDownByHourController extends ExceptionHandling {

    private final BreakDownByHourService breakDownByHourService;

    @Autowired
    public BreakDownByHourController(BreakDownByHourService breakDownByHourService) {
        this.breakDownByHourService = breakDownByHourService;
    }

    @PreAuthorize("hasAuthority('get-break-down-by-hour-by-discipline')")
    @GetMapping("/get-break-down-by-hour-by-discipline")
    public ResponseEntity<List<BreakDownByHourDtoResponse>> getBreakDownByHourByDiscipline(@RequestParam(name = "discipline_id") Long id) {
        List<BreakDownByHourDtoResponse> list = breakDownByHourService.getBreakDownByHourByDiscipline(id);
        return new ResponseEntity<>(list, OK);
    }

    @PreAuthorize("hasAuthority('get-break-down-by-hour-by-one')")
    @GetMapping("/get-break-down-by-hour-by-one")
    public ResponseEntity<BreakDownByHourDtoResponse> getBreakDownByHourByOne(@RequestParam(name = "breakDownByHourId") Long id) {
        BreakDownByHourDtoResponse breakDownByHourDtoResponse = breakDownByHourService.getBreakDownByHourByOne(id);
        return new ResponseEntity<>(breakDownByHourDtoResponse, OK);
    }

    @PreAuthorize("hasAuthority('create-break-down-by-hour')")
    @PostMapping("/create-break-down-by-hour")
    public ResponseEntity<HttpStatus> createBreakDownByHour(@RequestBody List<CreateBreakDownByHourDtoRequest> requestList, @RequestParam(name = "discipline_id") Long disciplineId, Principal principal) {
        breakDownByHourService.createBreakDownByHour(requestList, disciplineId, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasAuthority('update-break-down-by-hour')")
    @PutMapping("/update-break-down-by-hour")
    public ResponseEntity<HttpStatus> updateBreakDownByHour(@RequestBody List<UpdateBreakDownByHourDtoRequest> requestList, @RequestParam(name = "discipline_id") Long disciplineId, Principal principal) {
        breakDownByHourService.updateBreakDownByHour(requestList, disciplineId, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasAuthority('delete-break-down-by-hour')")
    @DeleteMapping("/delete-break-down-by-hour")
    public ResponseEntity<HttpStatus> deleteBreakDownByHour(@RequestParam(name = "discipline_id") Long disciplineId, Principal principal) {
        breakDownByHourService.deleteBreakDownByHour(disciplineId, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }
}
