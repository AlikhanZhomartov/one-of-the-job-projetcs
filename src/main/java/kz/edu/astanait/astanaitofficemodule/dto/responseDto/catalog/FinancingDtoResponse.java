package kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog;

import lombok.Data;

@Data
public class FinancingDtoResponse {
    private Long id;

    private String typeOfFinancing;
}
