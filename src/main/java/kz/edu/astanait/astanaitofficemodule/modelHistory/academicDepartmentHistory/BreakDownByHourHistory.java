package kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "z_break_down_by_hour_history")
public class BreakDownByHourHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "break_down_by_hour_id")
    private Long breakDownByHourId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "discipline_id")
    private Long disciplineId;

    @Column(name = "lessons_type_id")
    private Long lessonsTypeId;

    @Column(name = "hours")
    private Integer hours;

    @Column(name = "type_of_action")
    private String typeOfAction;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }
}
