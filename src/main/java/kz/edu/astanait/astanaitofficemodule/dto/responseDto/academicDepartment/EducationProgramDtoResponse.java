package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;


import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.AcademicDegreeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.DepartmentDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.EducationLanguageDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.GroupOfEducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.catalog.AcademicDegree;
import kz.edu.astanait.astanaitofficemodule.model.catalog.Department;
import kz.edu.astanait.astanaitofficemodule.model.catalog.EducationLanguage;
import kz.edu.astanait.astanaitofficemodule.model.catalog.GroupOfEducationProgram;
import lombok.Data;

import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.util.Date;

@Data
public class EducationProgramDtoResponse {
    private Long id;

    private String titleEn;

    private String titleRu;

    private String titleKz;

    private String descriptionEn;

    private String descriptionRu;

    private String descriptionKz;

    private String distinctiveFeaturesEn;

    private String distinctiveFeaturesRu;

    private String distinctiveFeaturesKz;

    private DepartmentDtoResponse department;

    private String typeOfEducationProgram;

    private String typeOfResponse;

    private String status;

    private String levelByNRK;

    private String levelByORK;

    private AcademicDegreeDtoResponse academicDegree;

    private LocalDate educationStartedDate;

    private Integer volumeCredits;

    private EducationLanguageDtoResponse educationLanguage;

    private GroupOfEducationProgramDtoResponse groupOfEducationProgram;

    private LocalDate approvalDateByAcademicCouncil;

    private Boolean license;

    private Boolean accreditation;
}
