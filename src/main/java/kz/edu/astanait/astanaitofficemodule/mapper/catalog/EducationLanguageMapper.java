package kz.edu.astanait.astanaitofficemodule.mapper.catalog;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.EducationLanguageDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.catalog.EducationLanguage;
import org.apache.logging.log4j.util.Strings;

public class EducationLanguageMapper {

    public EducationLanguageDtoResponse educationLanguageToDto(EducationLanguage educationLanguage) {
        EducationLanguageDtoResponse educationLanguageDtoResponse = new EducationLanguageDtoResponse();
        educationLanguageDtoResponse.setId(educationLanguage.getId());
        if (Strings.isNotBlank(educationLanguage.getTitleEn())) educationLanguageDtoResponse.setTitleEn(educationLanguage.getTitleEn());
        if (Strings.isNotBlank(educationLanguage.getTitleRu())) educationLanguageDtoResponse.setTitleRu(educationLanguage.getTitleRu());
        if (Strings.isNotBlank(educationLanguage.getTitleKz())) educationLanguageDtoResponse.setTitleKz(educationLanguage.getTitleKz());
        return educationLanguageDtoResponse;
    }
}
