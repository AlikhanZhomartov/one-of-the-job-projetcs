package kz.edu.astanait.astanaitofficemodule.mapper.catalog;


import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.LessonsTypeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.catalog.LessonsType;
import org.apache.logging.log4j.util.Strings;

public class LessonsTypeMapper {
    public LessonsTypeDtoResponse lessonsTypeToDto(LessonsType lessonsType) {
       LessonsTypeDtoResponse lessonsTypeDtoResponse = new LessonsTypeDtoResponse();
       lessonsTypeDtoResponse.setId(lessonsType.getId());
       if (Strings.isNotBlank(lessonsType.getTitleEn())) lessonsTypeDtoResponse.setTitleEn(lessonsType.getTitleEn());
       if (Strings.isNotBlank(lessonsType.getTitleRu())) lessonsTypeDtoResponse.setTitleRu(lessonsType.getTitleRu());
       if (Strings.isNotBlank(lessonsType.getTitleKz())) lessonsTypeDtoResponse.setTitleKz(lessonsType.getTitleKz());
       return lessonsTypeDtoResponse;
    }
}
