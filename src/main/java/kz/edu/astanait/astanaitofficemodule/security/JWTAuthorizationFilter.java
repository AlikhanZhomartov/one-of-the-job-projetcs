package kz.edu.astanait.astanaitofficemodule.security;


import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.auth.JWTDtoResponse;
import kz.edu.astanait.astanaitofficemodule.enviroment.JWTEnvironmentBuilder;
import kz.edu.astanait.astanaitofficemodule.feignClient.auth.AuthorizationServiceClient;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@Component
public class JWTAuthorizationFilter extends OncePerRequestFilter {

    private final JWTEnvironmentBuilder jwtEnvironmentBuilder;
    private final AuthorizationServiceClient authorizationServiceClient;

    @Autowired
    public JWTAuthorizationFilter(JWTEnvironmentBuilder jwtEnvironmentBuilder, AuthorizationServiceClient authorizationServiceClient) {
        this.jwtEnvironmentBuilder = jwtEnvironmentBuilder;
        this.authorizationServiceClient = authorizationServiceClient;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (Strings.isBlank(authorizationHeader) || !authorizationHeader.startsWith(jwtEnvironmentBuilder.getTOKEN_PREFIX())) {
            filterChain.doFilter(request, response);
            return;
        }
        try {
            JWTDtoResponse jwtDtoResponse = authorizationServiceClient.jwtTokenDecryption(authorizationHeader).getBody();
            if (jwtDtoResponse != null) {
                List<GrantedAuthority> authorities = new ArrayList<>();
                if (jwtDtoResponse.getAuthorities() != null) {
                    authorities = Arrays.stream(jwtDtoResponse.getAuthorities()).map(SimpleGrantedAuthority::new)
                            .collect(Collectors.toList());
                }
                Authentication authentication = getAuthentication(jwtDtoResponse.getUserId(), authorities, request);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception e) {
            SecurityContextHolder.clearContext();
            response.setStatus(UNAUTHORIZED.value());
            return;
        }
        filterChain.doFilter(request, response);
    }

    private Authentication getAuthentication(Long username, List<GrantedAuthority> authorities, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new
                UsernamePasswordAuthenticationToken(username, null, authorities);
        usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        return usernamePasswordAuthenticationToken;
    }
}
