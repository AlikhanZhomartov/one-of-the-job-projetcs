package kz.edu.astanait.astanaitofficemodule.controller.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.discipline.DisciplineDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.DisciplineDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionHandling;
import kz.edu.astanait.astanaitofficemodule.service.academicDepartment.DisciplineService;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;


@RestController
@RequestMapping("/api/v1/academic-department/discipline")
public class DisciplineController extends ExceptionHandling {

    private final DisciplineService disciplineService;

    @Autowired
    public DisciplineController(DisciplineService disciplineService) {
        this.disciplineService = disciplineService;
    }


    @PreAuthorize("hasAuthority('get-all-disciplines')")
    @GetMapping("/get-all-disciplines")
    public ResponseEntity<List<DisciplineDtoResponse>> getAllDisciplines() {
        return new ResponseEntity<>(disciplineService.getAllDisciplines(), OK);
    }

    @PreAuthorize("hasAuthority('get-disciplines-by-30')")
    @GetMapping("/get-disciplines-by-30")
    public ResponseEntity<HashMap<String, Object>> getDisciplinesBy30(@RequestParam(name = "page", defaultValue = "0") int page) {
        return new ResponseEntity<>(disciplineService.getDisciplinesBy30(page), OK);
    }

    @PreAuthorize("hasAuthority('get-disciplines-by-filter')")
    @GetMapping("/get-disciplines-by-filter")
    public ResponseEntity<HashMap<String, Object>> getDisciplineByFilter(@RequestParam(name = "page", defaultValue = "0") int page,
                                                                         @RequestParam(name = "departmentId", defaultValue = "0") Long departmentId,
                                                                         @RequestParam(name = "academicDegreeId", defaultValue = "0") Long academicDegreeId,
                                                                         @RequestParam(name = "keyword", defaultValue = "0") String keyword) {
        return new ResponseEntity<>(disciplineService.getDisciplinesByFilter(departmentId, academicDegreeId, keyword, page), OK);
    }

    @PreAuthorize("hasAuthority('get-disciplines-by-teacher')")
    @GetMapping("/get-disciplines-by-teacher")
    public ResponseEntity<List<DisciplineDtoResponse>> getDisciplinesByTeacher(@RequestParam(name = "teacher_id") Long teacherId) {
        return new ResponseEntity<>(disciplineService.getDisciplinesByTeacher(teacherId), OK);
    }

    @PreAuthorize("hasAuthority('get-discipline-by-one')")
    @GetMapping("/get-discipline-by-one")
    public ResponseEntity<DisciplineDtoResponse> getDisciplineByOne(@RequestParam(name = "discipline_id") Long disciplineId) {
        return new ResponseEntity<>(disciplineService.getDisciplineByOne(disciplineId), OK);
    }

    @PreAuthorize("hasAuthority('create-discipline')")
    @PostMapping("/create-discipline")
    public ResponseEntity<Long> createDiscipline(@Valid @RequestBody DisciplineDtoRequest disciplineDtoRequest, Principal principal) {
        Long id = disciplineService.createDiscipline(disciplineDtoRequest, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(id, OK);
    }

    @PreAuthorize("hasAuthority('update-discipline')")
    @PutMapping("/update-discipline")
    public ResponseEntity<HttpStatus> updateDiscipline(@RequestBody DisciplineDtoRequest disciplineDtoRequest, @RequestParam(name = "discipline_id") Long disciplineId, Principal principal) {
        disciplineService.updateDiscipline(disciplineDtoRequest, disciplineId, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasAuthority('delete-discipline')")
    @DeleteMapping("/delete-discipline")
    public ResponseEntity<HttpStatus> deleteDiscipline(@RequestParam(name = "discipline_id") Long disciplineId, Principal principal) {
        disciplineService.deleteDiscipline(disciplineId, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }



}
