package kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.Curriculum;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CurriculumByGroupDtoRequest {
    @NotNull(message = "образовательная программа должна быть выбрана!")
    private Long educationProgramId;

    @NotNull(message = "Дисциплины должны быть выбраны!")
    private List<Long> disciplineId;

    @NotNull(message = "Номер триместра должен быть выбран!")
    private Integer numberOfTrimester;
}
