package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.CurriculumDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.DisciplineDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Curriculum;

import java.util.Objects;

public class CurriculumMapper {

    public CurriculumDtoResponse curriculumToDto(Curriculum curriculum) {
        CurriculumDtoResponse curriculumDtoResponse = new CurriculumDtoResponse();
        curriculumDtoResponse.setId(curriculum.getId());
        if (Objects.nonNull(curriculum.getEducationProgram())) curriculumDtoResponse.setEducationProgram(new EducationProgramMapper().educationProgramToDto(curriculum.getEducationProgram()));
        if (Objects.nonNull(curriculum.getDiscipline())) curriculumDtoResponse.setDiscipline(new DisciplineMapper().disciplineToDto(curriculum.getDiscipline()));
        if (Objects.nonNull(curriculum.getNumberOfTrimester())) curriculumDtoResponse.setNumberOfTrimester(curriculum.getNumberOfTrimester());
        if (Objects.nonNull(curriculum.getGroupedDisciplines())) curriculumDtoResponse.setGroupedDisciplines(curriculum.getGroupedDisciplines());
        return curriculumDtoResponse;
    }
}
