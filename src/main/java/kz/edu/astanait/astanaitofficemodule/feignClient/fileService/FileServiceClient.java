package kz.edu.astanait.astanaitofficemodule.feignClient.fileService;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AssessmentReport.SummarySheetDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AssessmentReport.SummarySheetForTranscriptDtoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.List;

@FeignClient(name = "file-service-2")
public interface FileServiceClient {

    @PostMapping("/api/v1/pdf/transcript")
    ResponseEntity<byte[]> getTranscript(@RequestBody HashMap<Integer, List<SummarySheetForTranscriptDtoResponse>> transcriptMap);

    @PostMapping("/api/v1/excel/summaryBill")
    ResponseEntity<byte[]> getSummaryBill(@RequestBody SummarySheetDtoResponse summarySheet);

    @PostMapping("/api/v1/excel/semesterBill")
    ResponseEntity<byte[]> getSemesterBill(@RequestBody List<SummarySheetDtoResponse> summarySheets);
}
