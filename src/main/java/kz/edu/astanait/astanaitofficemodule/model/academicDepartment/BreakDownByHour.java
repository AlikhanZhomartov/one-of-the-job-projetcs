package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.catalog.LessonsType;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "break_down_by_hour")
public class BreakDownByHour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "discipline_id")
    private Discipline discipline;

    @ManyToOne
    @JoinColumn(name = "lessons_type_id")
    private LessonsType lessonsType;

    @Column(name = "hours")
    private Integer hours;
}
