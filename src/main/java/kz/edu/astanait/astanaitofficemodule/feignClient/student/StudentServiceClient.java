package kz.edu.astanait.astanaitofficemodule.feignClient.student;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student.GroupDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student.StudentDtoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "astanait-student-module")
public interface StudentServiceClient {

    @GetMapping("/api/v1/student/get-student-by-group")
    ResponseEntity<List<StudentDtoResponse>> getStudentsByGroup(@RequestHeader("Authorization") String token, @RequestParam(name = "group_id") Long groupId);

    @GetMapping("/api/v1/student/get-student-by-id")
    ResponseEntity<StudentDtoResponse> getStudentsById(@RequestHeader("Authorization") String token, @RequestParam(name = "student_id") Long student_id);

    @GetMapping("/api/v1/student/get-student-by-user-id")
    ResponseEntity<StudentDtoResponse> getStudentsByUserId(@RequestHeader("Authorization") String token, @RequestParam(name = "user_id") Long userId);

    @GetMapping("/api/v1/group/get-all")
    ResponseEntity<List<GroupDtoResponse>> getAllGroup(@RequestHeader("Authorization") String token);

}
