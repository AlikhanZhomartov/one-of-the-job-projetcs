package kz.edu.astanait.astanaitofficemodule.controller.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AssessmentReport.SummarySheetDtoResponse;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.AssessmentReport.SummarySheetForTranscriptDtoResponse;
import kz.edu.astanait.astanaitofficemodule.service.academicDepartment.AssessmentReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/v1/academic-department/assessment-report")
public class AssessmentReportController {

    private final AssessmentReportService assessmentReportService;

    @Autowired
    public AssessmentReportController(AssessmentReportService assessmentReportService) {
        this.assessmentReportService = assessmentReportService;
    }

    @PreAuthorize("hasAuthority('summary-sheet-by-discipline-and-group')")
    @GetMapping("/summary-sheet-by-discipline-and-group")
    public ResponseEntity<SummarySheetDtoResponse> summarySheetByDisciplineAndGroup(@RequestParam(name = "discipline_id") Long disciplineId,
                                                                                    @RequestParam(name = "teacherId_id") Long teacherId,
                                                                                    @RequestParam(name = "group_id") Long groupId,
                                                                                    HttpServletRequest request) {
       return new ResponseEntity<>(assessmentReportService.summarySheetByDisciplineAndGroup(disciplineId, teacherId, groupId, request), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('summary-sheet-by-term-and-group')")
    @GetMapping("/summary-sheet-by-term-and-group")
    public ResponseEntity<List<SummarySheetDtoResponse>> summarySheetByTermAndGroup(@RequestParam(name = "group_id") Long groupId,
                                                                                    @RequestParam(name = "term") Integer term,
                                                                                    @RequestParam(name = "year") Integer year,
                                                                                    HttpServletRequest request) {
        return new ResponseEntity<>(assessmentReportService.summarySheetByTermAndGroup(groupId, term, year, request), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('summary-sheet-by-for-transcript')")
    @GetMapping("/summary-sheet-by-for-transcript")
    public ResponseEntity<HashMap<Integer, List<SummarySheetForTranscriptDtoResponse>>> summarySheetForTranscript(@RequestParam(name = "user_id") Long userId, HttpServletRequest request) {
        return new ResponseEntity<>(assessmentReportService.summarySheetForTranscript(userId, request), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('summary-sheet-by-for-transcript-for-student')")
    @GetMapping("/summary-sheet-by-for-transcript-for-student")
    public ResponseEntity<HashMap<Integer, List<SummarySheetForTranscriptDtoResponse>>> summarySheetForTranscript(Principal principal, HttpServletRequest request) {
        return new ResponseEntity<>(assessmentReportService.summarySheetForTranscript(Long.parseLong(principal.getName()), request), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('summary-sheet-by-for-transcript-in-pdf')")
    @GetMapping("/summary-sheet-by-for-transcript-by-pdf")
    public ResponseEntity<byte[]> summarySheetForTranscriptForPDF(@RequestParam(name = "user_id") Long userId, HttpServletRequest request) {
        return new ResponseEntity<>(assessmentReportService.summarySheetForTranscriptInPDF(userId, request), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('summary-sheet-by-discipline-and-group-in-excel')")
    @GetMapping("/summary-sheet-by-discipline-and-group-in-excel")
    public ResponseEntity<byte[]> summarySheetByDisciplineAndGroupInExcel(@RequestParam(name = "discipline_id") Long disciplineId,
                                                                          @RequestParam(name = "teacherId_id") Long teacherId,
                                                                          @RequestParam(name = "group_id") Long groupId,
                                                                          HttpServletRequest request) {
        return new ResponseEntity<>(assessmentReportService.summarySheetByDisciplineAndGroupInExcel(disciplineId, teacherId, groupId, request), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('summary-sheet-by-term-and-group-in-excel')")
    @GetMapping("/summary-sheet-by-term-and-group-in-excel")
    public ResponseEntity<byte[]> summarySheetByTermAndGroupInExcel(@RequestParam(name = "group_id") Long groupId,
                                                                          @RequestParam(name = "term") Integer term,
                                                                          @RequestParam(name = "year") Integer year,
                                                                          HttpServletRequest request) {
        return new ResponseEntity<>(assessmentReportService.summarySheetByTermAndGroupInExcel(groupId, term, year, request), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('all-summary-sheet-by-term-and-group-in-excel')")
    @GetMapping("/all-summary-sheet-by-term-and-group-in-excel")
    public ResponseEntity<HttpStatus> allSummarySheetByTermAndGroupInExcel(@RequestParam(name = "term") Integer term,
                                                                           @RequestParam(name = "year") Integer year,
                                                                           HttpServletRequest request) {
        assessmentReportService.allSummarySheetByTermAndGroupInExcel(term, year, request);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
