package kz.edu.astanait.astanaitofficemodule.controller.catalog;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.AcademicDegreeDtoResponse;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.AcademicDegreeRepository;
import kz.edu.astanait.astanaitofficemodule.service.catalogService.AcademicDegreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/catalog/academic-degree")
public class AcademicDegreeController {

    private final AcademicDegreeService academicDegreeService;

    @Autowired
    public AcademicDegreeController(AcademicDegreeService academicDegreeService) {
        this.academicDegreeService = academicDegreeService;
    }

    @PreAuthorize("hasAuthority('get-all-academic-degrees')")
    @GetMapping("/get-all-academic-degrees")
    public ResponseEntity<List<AcademicDegreeDtoResponse>> getAllAcademicDegrees() {
        return new ResponseEntity<>(academicDegreeService.getAllAcademicDegrees(), HttpStatus.OK);
    }

}
