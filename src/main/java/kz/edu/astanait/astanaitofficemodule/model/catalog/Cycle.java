package kz.edu.astanait.astanaitofficemodule.model.catalog;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "cycle")
public class Cycle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "cycle_title_en")
    private String cycleTitleEn;

    @Column(name = "cycle_title_ru")
    private String cycleTitleRu;

    @Column(name = "cycle_title_kz")
    private String cycleTitleKz;

    @Column(name = "cycle_abbreviation_en")
    private String cycleAbbreviationEn;

    @Column(name = "cycle_abbreviation_ru")
    private String cycleAbbreviationRu;

    @Column(name = "cycle_abbreviation_kz")
    private String cycleAbbreviationKz;
}
