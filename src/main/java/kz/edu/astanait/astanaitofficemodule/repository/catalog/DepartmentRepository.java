package kz.edu.astanait.astanaitofficemodule.repository.catalog;

import kz.edu.astanait.astanaitofficemodule.model.catalog.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
