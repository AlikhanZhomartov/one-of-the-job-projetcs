package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.StudentDisciplineFinalChoose;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentDisciplineFinalChooseRepository extends JpaRepository<StudentDisciplineFinalChoose, Long> {
}
