package kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.breakDownByHour;

import lombok.Data;

@Data
public class CreateBreakDownByHourDtoRequest {
    private Long lessonsTypeId;
    private Integer hours;
}
