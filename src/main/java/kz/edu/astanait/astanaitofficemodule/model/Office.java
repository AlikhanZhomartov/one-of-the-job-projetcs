package kz.edu.astanait.astanaitofficemodule.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "offices")
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;
}
