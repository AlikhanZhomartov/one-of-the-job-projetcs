package kz.edu.astanait.astanaitofficemodule.repositoryHistory.academicDepartmentHistory;

import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.CurriculumHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurriculumHistoryRepository extends JpaRepository<CurriculumHistory, Long> {
}
