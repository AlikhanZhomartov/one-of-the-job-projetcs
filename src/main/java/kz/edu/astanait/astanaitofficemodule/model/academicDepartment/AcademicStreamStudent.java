package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "academic_stream_students")
public class AcademicStreamStudent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "academic_stream_id")
    private AcademicStream academicStream;

    @Column(name = "student_id")
    private Long studentId;

    @OneToMany(mappedBy = "academicStreamStudent")
    private List<AcademicStreamStudentGrade> academicStreamStudentGrades;
}
