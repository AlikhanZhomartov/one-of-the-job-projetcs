package kz.edu.astanait.astanaitofficemodule.repository;


import kz.edu.astanait.astanaitofficemodule.model.Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeRepository extends JpaRepository<Office, Long> {
    @Query(nativeQuery = true, value = "select max(id) from offices")
    Long selectMaxId();
}
