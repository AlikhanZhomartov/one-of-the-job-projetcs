package kz.edu.astanait.astanaitofficemodule.service.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.breakDownByHour.CreateBreakDownByHourDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.breakDownByHour.UpdateBreakDownByHourDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.BreakDownByHourDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.CustomNotFoundException;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.BreakDownByHourMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.BreakDownByHour;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Discipline;
import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.BreakDownByHourHistory;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.BreakDownByHourRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.DisciplineRepository;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.LessonsTypeRepository;
import kz.edu.astanait.astanaitofficemodule.serviceHistory.academicDepartmentHistory.BreakDownByHourHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BreakDownByHourService {

    private final BreakDownByHourRepository breakDownByHourRepository;
    private final DisciplineRepository disciplineRepository;
    private final LessonsTypeRepository lessonsTypeRepository;
    private final BreakDownByHourHistoryService breakDownByHourHistoryService;

    private static final int hoursByCredits = 30;


    @Autowired
    public BreakDownByHourService(BreakDownByHourRepository breakDownByHourRepository, DisciplineRepository disciplineRepository, LessonsTypeRepository lessonsTypeRepository, BreakDownByHourHistoryService breakDownByHourHistoryService) {
        this.breakDownByHourRepository = breakDownByHourRepository;
        this.disciplineRepository = disciplineRepository;
        this.lessonsTypeRepository = lessonsTypeRepository;
        this.breakDownByHourHistoryService = breakDownByHourHistoryService;
    }

    public List<BreakDownByHourDtoResponse> getBreakDownByHourByDiscipline(Long disciplineId) {
        return breakDownByHourRepository.findAllByDisciplineId(disciplineId)
                .stream().map(v -> new BreakDownByHourMapper().downByHourToDto(v)).collect(Collectors.toList());
    }

    public BreakDownByHourDtoResponse getBreakDownByHourByOne(Long breakDownByHourId) {
        BreakDownByHour breakDownByHour = breakDownByHourRepository.findById(breakDownByHourId)
                .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "breakDownByHour", "id")));
        return new BreakDownByHourMapper().downByHourToDto(breakDownByHour);
    }

    public void createBreakDownByHour(List<CreateBreakDownByHourDtoRequest> requestList, Long disciplineId, Long userId) {
        Discipline discipline = disciplineRepository.findById(disciplineId)
                .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Discipline", "id")));
        int totalHours = requestList.stream().mapToInt(CreateBreakDownByHourDtoRequest::getHours).sum();
        if (discipline.getVolumeCredits() * hoursByCredits != totalHours) throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "breakDownByHour", TypeOfActionForHistory.CREATE));
        for (CreateBreakDownByHourDtoRequest request : requestList) {
            BreakDownByHour breakDownByHour = new BreakDownByHour();
            breakDownByHour.setDiscipline(discipline);
            breakDownByHour.setLessonsType(lessonsTypeRepository.findById(request.getLessonsTypeId()).orElseThrow(() ->
                    new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Lesson type", "id"))));
            breakDownByHour.setHours(request.getHours());
            try {
                BreakDownByHour createdBreakDownByHour = breakDownByHourRepository.save(breakDownByHour);
                breakDownByHourHistoryService.createBreakDownByHourHistory(createdBreakDownByHour, userId);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "breakDownByHour", TypeOfActionForHistory.CREATE));
            }
        }
    }

    @Transactional
    public void updateBreakDownByHour(List<UpdateBreakDownByHourDtoRequest> request, Long disciplineId, Long userId) {
        Discipline discipline = disciplineRepository.findById(disciplineId)
                .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Discipline", "id")));

        List<BreakDownByHour> breakDownByHourList = breakDownByHourRepository.findAllByDisciplineId(disciplineId);
        int totalHours = 0;
        for (BreakDownByHour breakDownByHour : breakDownByHourList) {
            totalHours += breakDownByHour.getHours();
        }

        for (BreakDownByHour breakDownByHour : breakDownByHourList) {
            for (UpdateBreakDownByHourDtoRequest dtoRequest : request) {
                if (dtoRequest.getBreakDownByHourId().equals(breakDownByHour.getId())) {
                    totalHours -= breakDownByHour.getHours();
                    totalHours += dtoRequest.getHours();
                }
            }
        }

        if (discipline.getVolumeCredits() * hoursByCredits != totalHours) throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "breakDownByHour", TypeOfActionForHistory.UPDATE));


        for (UpdateBreakDownByHourDtoRequest requestList : request) {
            BreakDownByHour breakDownByHour = breakDownByHourRepository.findById(requestList.getBreakDownByHourId())
                    .orElseThrow(() -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "breakDownByHour", "id")));

            breakDownByHourHistoryService.updateBreakDownByHourHistory(breakDownByHour, userId);

            breakDownByHour.setLessonsType(lessonsTypeRepository.findById(requestList.getLessonsTypeId()).orElseThrow(() ->
                    new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Lesson type", "id"))));
            breakDownByHour.setHours(requestList.getHours());
            try {
                breakDownByHourRepository.save(breakDownByHour);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "breakDownByHour", TypeOfActionForHistory.UPDATE));
            }
        }
    }

    public void deleteBreakDownByHour(Long disciplineId, Long userId) {
        try {
            breakDownByHourRepository.deleteByDisciplineId(disciplineId);
            breakDownByHourHistoryService.deleteBreakDownByHourHistory(disciplineId, userId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "breakDownByHour", TypeOfActionForHistory.DELETE));
        }
    }
}
