package kz.edu.astanait.astanaitofficemodule.serviceHistory.academicDepartmentHistory;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Curriculum;
import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.CurriculumHistory;
import kz.edu.astanait.astanaitofficemodule.repositoryHistory.academicDepartmentHistory.CurriculumHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class CurriculumHistoryService {

    private final CurriculumHistoryRepository curriculumHistoryRepository;

    @Autowired
    public CurriculumHistoryService(CurriculumHistoryRepository curriculumHistoryRepository) {
        this.curriculumHistoryRepository = curriculumHistoryRepository;
    }

    private void manipulateCurriculumHistory(Curriculum curriculum, Long userId, String action) {
        CurriculumHistory curriculumHistory = new CurriculumHistory();
        curriculumHistory.setCurriculumId(curriculum.getId());
        curriculumHistory.setUserId(userId);
        if (Objects.nonNull(curriculum.getEducationProgram())) curriculumHistory.setEducationProgramId(curriculum.getEducationProgram().getId());
        if (Objects.nonNull(curriculum.getDiscipline())) curriculumHistory.setDisciplineId(curriculum.getDiscipline().getId());
        if (Objects.nonNull(curriculum.getNumberOfTrimester())) curriculumHistory.setNumberOfTrimester(curriculum.getNumberOfTrimester());
        if (Objects.nonNull(curriculum.getGroupedDisciplines())) curriculumHistory.setGroupedDisciplines(curriculum.getGroupedDisciplines());
        curriculumHistory.setTypeOfAction(action);
        try {
            curriculumHistoryRepository.save(curriculumHistory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createCurriculumHistory(Curriculum curriculum,  Long userId) {
        manipulateCurriculumHistory(curriculum, userId, TypeOfActionForHistory.CREATE);
    }

    public void deleteCurriculumHistory(Curriculum curriculum, Long userId) {
        manipulateCurriculumHistory(curriculum, userId, TypeOfActionForHistory.DELETE);
    }

}
