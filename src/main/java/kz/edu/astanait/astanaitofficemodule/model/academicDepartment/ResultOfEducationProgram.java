package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "result_of_education_program")
public class ResultOfEducationProgram {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "result_en")
    private String resultEn;

    @Column(name = "result_ru")
    private String resultRu;

    @Column(name = "result_kz")
    private String resultKz;

    @Column(name = "code")
    private String code;

    @ManyToOne
    @JoinColumn(name = "education_program_id")
    private EducationProgram educationProgram;
}
