package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.PreviousCurriculumDiscipline;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PreviousCurriculumDisciplineRepository extends JpaRepository<PreviousCurriculumDiscipline, Long> {
    List<PreviousCurriculumDiscipline> findByCurriculumId(Long id);
    List<PreviousCurriculumDiscipline> findByPreviousCurriculumDisciplineId(Long id);
    void deleteByCurriculumId(Long id);
}
