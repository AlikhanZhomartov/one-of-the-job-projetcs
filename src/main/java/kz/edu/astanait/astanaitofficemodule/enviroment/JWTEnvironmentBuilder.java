package kz.edu.astanait.astanaitofficemodule.enviroment;

import lombok.Data;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Data
public class JWTEnvironmentBuilder {
    private final String TOKEN_PREFIX;

    public JWTEnvironmentBuilder(Environment environment) {
        this.TOKEN_PREFIX = environment.getRequiredProperty("security.token.token-prefix");
    }
}
