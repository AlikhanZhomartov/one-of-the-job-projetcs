package kz.edu.astanait.astanaitofficemodule.service.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.PreviousCurriculumDisciplineDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.PreviousCurriculumDisciplineMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.PreviousCurriculumDiscipline;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.CurriculumRepository;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.PreviousCurriculumDisciplineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PreviousCurriculumDisciplineService {

    private final PreviousCurriculumDisciplineRepository previousCurriculumDisciplineRepository;
    private final CurriculumRepository curriculumRepository;

    @Autowired
    public PreviousCurriculumDisciplineService(PreviousCurriculumDisciplineRepository previousCurriculumDisciplineRepository, CurriculumRepository curriculumRepository) {
        this.previousCurriculumDisciplineRepository = previousCurriculumDisciplineRepository;
        this.curriculumRepository = curriculumRepository;
    }

    public List<PreviousCurriculumDisciplineDtoResponse> getPreviousCurriculumDisciplineByCurriculum(Long curriculumId) {
        List<PreviousCurriculumDisciplineDtoResponse> list = new ArrayList<>();
        PreviousCurriculumDisciplineMapper previousCurriculumDisciplineMapper = new PreviousCurriculumDisciplineMapper();
        previousCurriculumDisciplineRepository.findByCurriculumId(curriculumId).forEach(v -> {
            list.add(previousCurriculumDisciplineMapper.previousCurriculumDisciplineToDto(v));
        });

        previousCurriculumDisciplineRepository.findByPreviousCurriculumDisciplineId(curriculumId).forEach(v -> {
            list.add(previousCurriculumDisciplineMapper.previousCurriculumDisciplineToDto(v));
        });
        return list;
    }

    public Long createPreviousCurriculumDiscipline(Long curriculumId, Long previousCurriculumDisciplineId) {
        PreviousCurriculumDiscipline previousCurriculumDiscipline = new PreviousCurriculumDiscipline();
        curriculumRepository.findById(curriculumId).ifPresent(previousCurriculumDiscipline::setCurriculum);
        curriculumRepository.findById(previousCurriculumDisciplineId).ifPresent(previousCurriculumDiscipline::setPreviousCurriculumDiscipline);
        try {
            return previousCurriculumDisciplineRepository.save(previousCurriculumDiscipline).getId();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "previous curriculum discipline", TypeOfActionForHistory.CREATE));
        }
    }

    public void deletePreviousCurriculumDiscipline(Long curriculumId) {
        try {
            previousCurriculumDisciplineRepository.deleteByCurriculumId(curriculumId);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "previous curriculum discipline", TypeOfActionForHistory.DELETE));
        }
    }

}
