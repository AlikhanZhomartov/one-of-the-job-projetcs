package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "academic_stream_students_grades")
public class AcademicStreamStudentGrade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "academic_stream_student_id")
    private AcademicStreamStudent academicStreamStudent;

    @Column(name = "grade")
    private Float grade;

    @Column(name = "grade_type")
    private String gradeType;

    @Column(name = "approved")
    private Boolean approved;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }
}
