package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;


import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.DisciplineDtoResponse;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.AcademicDegreeMapper;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.DepartmentMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.Discipline;
import org.apache.logging.log4j.util.Strings;

import java.util.Objects;

public class DisciplineMapper {

    public DisciplineDtoResponse disciplineToDto(Discipline discipline) {
        DisciplineDtoResponse disciplineDtoResponse = new DisciplineDtoResponse();
        disciplineDtoResponse.setId(discipline.getId());
        if (Strings.isNotBlank(discipline.getTitleEn())) disciplineDtoResponse.setTitleEn(discipline.getTitleEn());
        if (Strings.isNotBlank(discipline.getTitleRu())) disciplineDtoResponse.setTitleRu(discipline.getTitleRu());
        if (Strings.isNotBlank(discipline.getTitleKz())) disciplineDtoResponse.setTitleKz(discipline.getTitleKz());
        if (Strings.isNotBlank(discipline.getDescriptionEn())) disciplineDtoResponse.setDescriptionEn(discipline.getDescriptionEn());
        if (Strings.isNotBlank(discipline.getDescriptionRu())) disciplineDtoResponse.setDescriptionRu(discipline.getDescriptionRu());
        if (Strings.isNotBlank(discipline.getDescriptionKz())) disciplineDtoResponse.setDescriptionKz(discipline.getDescriptionKz());
        if (Objects.nonNull(discipline.getVolumeCredits())) disciplineDtoResponse.setVolumeCredits(discipline.getVolumeCredits());
        if (Strings.isNotBlank(discipline.getCode())) disciplineDtoResponse.setCode(discipline.getCode());
        if (Objects.nonNull(discipline.getDepartment())) disciplineDtoResponse.setDepartment(new DepartmentMapper().departmentToDto(discipline.getDepartment()));
        if (Objects.nonNull(discipline.getAcademicDegree())) disciplineDtoResponse.setAcademicDegree(new AcademicDegreeMapper().academicDegreeToDto(discipline.getAcademicDegree()));
        return disciplineDtoResponse;
    }

}
