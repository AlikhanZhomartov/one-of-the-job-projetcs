package kz.edu.astanait.astanaitofficemodule.model.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.catalog.AcademicDegree;
import kz.edu.astanait.astanaitofficemodule.model.catalog.Department;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "discipline")
public class Discipline {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title_en")
    private String titleEn;

    @Column(name = "title_ru")
    private String titleRu;

    @Column(name = "title_kz")
    private String titleKz;

    @Column(name = "description_en")
    private String descriptionEn;

    @Column(name = "description_ru")
    private String descriptionRu;

    @Column(name = "description_kz")
    private String descriptionKz;

    @Column(name = "code")
    private String code;

    @Column(name = "volume_credits")
    private Integer volumeCredits;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToOne
    @JoinColumn(name = "academic_degree_id")
    private AcademicDegree academicDegree;
}
