package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import lombok.Data;

@Data
public class CurriculumDtoResponse {

    private Long id;

    private EducationProgramDtoResponse educationProgram;

    private DisciplineDtoResponse discipline;

    private Integer numberOfTrimester;

    private Long groupedDisciplines;
}
