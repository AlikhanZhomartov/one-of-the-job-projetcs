package kz.edu.astanait.astanaitofficemodule.serviceHistory.academicDepartmentHistory;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.EducationProgram;
import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.EducationProgramHistory;
import kz.edu.astanait.astanaitofficemodule.repositoryHistory.academicDepartmentHistory.EducationProgramHistoryRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class EducationProgramHistoryService {

    private final EducationProgramHistoryRepository educationProgramHistoryRepository;

    @Autowired
    public EducationProgramHistoryService(EducationProgramHistoryRepository educationProgramHistoryRepository) {
        this.educationProgramHistoryRepository = educationProgramHistoryRepository;
    }

    public void createEducationProgram(EducationProgram educationProgram, Long userId) {
        manipulateEducationProgramHistory(educationProgram, userId, TypeOfActionForHistory.CREATE);
    }

    public void updateEducationProgram(EducationProgram educationProgram, Long userId) {
        manipulateEducationProgramHistory(educationProgram, userId, TypeOfActionForHistory.UPDATE);
    }

    public void deleteEducationProgram(EducationProgram educationProgram, Long userId) {
        manipulateEducationProgramHistory(educationProgram, userId, TypeOfActionForHistory.DELETE);
    }

    private void manipulateEducationProgramHistory(EducationProgram educationProgram, Long userId, String action) {
        EducationProgramHistory educationProgramHistory = new EducationProgramHistory();
        educationProgramHistory.setEducationProgramId(educationProgram.getId());
        educationProgramHistory.setUserId(userId);
        if (Strings.isNotBlank(educationProgram.getTitleEn())) educationProgramHistory.setTitleEn(educationProgram.getTitleEn());
        if (Strings.isNotBlank(educationProgram.getTitleRu())) educationProgramHistory.setTitleRu(educationProgram.getTitleRu());
        if (Strings.isNotBlank(educationProgram.getTitleKz())) educationProgramHistory.setTitleKz(educationProgram.getTitleKz());
        if (Strings.isNotBlank(educationProgram.getDescriptionEn())) educationProgramHistory.setDescriptionEn(educationProgram.getDescriptionEn());
        if (Strings.isNotBlank(educationProgram.getDescriptionRu())) educationProgramHistory.setDescriptionRu(educationProgram.getDescriptionRu());
        if (Strings.isNotBlank(educationProgram.getDescriptionKz())) educationProgramHistory.setDescriptionKz(educationProgram.getDescriptionKz());
        if (Strings.isNotBlank(educationProgram.getDistinctiveFeaturesEn())) educationProgramHistory.setDistinctiveFeaturesEn(educationProgram.getDistinctiveFeaturesEn());
        if (Strings.isNotBlank(educationProgram.getDistinctiveFeaturesRu())) educationProgramHistory.setDistinctiveFeaturesRu(educationProgram.getDistinctiveFeaturesRu());
        if (Strings.isNotBlank(educationProgram.getDistinctiveFeaturesKz())) educationProgramHistory.setDistinctiveFeaturesKz(educationProgram.getDistinctiveFeaturesKz());
        if (Objects.nonNull(educationProgram.getDepartment())) educationProgramHistory.setDepartmentId(educationProgram.getDepartment().getId());
        if (Strings.isNotBlank(educationProgram.getTypeOfEducationProgram())) educationProgramHistory.setTypeOfEducationProgram(educationProgram.getTypeOfEducationProgram());
        if (Strings.isNotBlank(educationProgram.getTypeOfResponse())) educationProgramHistory.setTypeOfResponse(educationProgram.getTypeOfResponse());
        if (Strings.isNotBlank(educationProgram.getStatus())) educationProgramHistory.setStatus(educationProgram.getStatus());
        if (Strings.isNotBlank(educationProgram.getLevelByNRK())) educationProgramHistory.setLevelByNRK(educationProgram.getLevelByNRK());
        if (Strings.isNotBlank(educationProgram.getLevelByORK())) educationProgramHistory.setLevelByORK(educationProgram.getLevelByORK());
        if (Objects.nonNull(educationProgram.getAcademicDegree())) educationProgramHistory.setAcademicDegreeId(educationProgram.getAcademicDegree().getId());
        if (Objects.nonNull(educationProgram.getEducationStartedDate())) educationProgramHistory.setEducationStartedDate(educationProgram.getEducationStartedDate());
        if (Objects.nonNull(educationProgram.getVolumeCredits())) educationProgramHistory.setVolumeCredits(educationProgram.getVolumeCredits());
        if (Objects.nonNull(educationProgram.getEducationLanguage())) educationProgramHistory.setEducationLanguageId(educationProgram.getEducationLanguage().getId());
        if (Objects.nonNull(educationProgram.getGroupOfEducationProgram())) educationProgramHistory.setGroupOfEducationProgramId(educationProgram.getGroupOfEducationProgram().getId());
        if (Objects.nonNull(educationProgram.getApprovalDateByAcademicCouncil())) educationProgramHistory.setApprovalDateByAcademicCouncil(educationProgram.getApprovalDateByAcademicCouncil());
        if (Objects.nonNull(educationProgram.getLicense())) educationProgram.setLicense(educationProgram.getLicense());
        if (Objects.nonNull(educationProgram.getAccreditation())) educationProgram.setAccreditation(educationProgram.getAccreditation());
        educationProgramHistory.setTypeOfAction(action);
        try {
            educationProgramHistoryRepository.save(educationProgramHistory);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
