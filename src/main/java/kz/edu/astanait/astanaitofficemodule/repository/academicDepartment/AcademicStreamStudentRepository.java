package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.AcademicStreamStudent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcademicStreamStudentRepository extends JpaRepository<AcademicStreamStudent, Long> {
    @Query(value = "select ss.id, academic_stream_id, student_id from academic_stream_students ss inner join academic_stream_students_grades assg on ss.id = assg.academic_stream_student_id\n" +
            "inner join students s on ss.student_id = s.id inner join academic_stream \"as\" on \"as\".id = ss.academic_stream_id where s.id in (?1)  and \"as\".discipline_id = ?2 and \"as\".teacher_id = ?3 group by ss.id", nativeQuery = true)
    List<AcademicStreamStudent> findByAcademicStreamStudentGroupAndDiscipline(List<Long> studentsId, Long disciplineId, Long teacherId);
    List<AcademicStreamStudent> findAllByStudentId(Long id);
}
