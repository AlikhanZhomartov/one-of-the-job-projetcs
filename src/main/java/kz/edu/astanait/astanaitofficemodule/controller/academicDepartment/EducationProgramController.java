package kz.edu.astanait.astanaitofficemodule.controller.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.EducationProgram.EducationProgramDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.EducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionHandling;
import kz.edu.astanait.astanaitofficemodule.service.academicDepartment.EducationProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/v1/academic-department/education-program")
public class EducationProgramController extends ExceptionHandling {

    private final EducationProgramService educationProgramService;

    @Autowired
    public EducationProgramController(EducationProgramService educationProgramService) {
        this.educationProgramService = educationProgramService;
    }

    @PreAuthorize("hasAuthority('get-all-education-programs')")
    @GetMapping("/get-all-education-programs")
    public ResponseEntity<List<EducationProgramDtoResponse>> getAllEducationPrograms() {
        return new ResponseEntity<>(educationProgramService.getAllEducationPrograms(), OK);
    }

    @PreAuthorize("hasAuthority('get-education-program-by-one')")
    @GetMapping("/get-education-program-by-one")
    public ResponseEntity<EducationProgramDtoResponse> getEducationProgramByOne(@RequestParam(name = "education_program_id") Long educationProgramId) {
        return new ResponseEntity<>(educationProgramService.getEducationProgramByOne(educationProgramId), OK);
    }

    @PreAuthorize("hasAuthority('create-education-program')")
    @PostMapping("/create-education-program")
    public ResponseEntity<Long> createEducationProgram(@Valid @RequestBody EducationProgramDtoRequest educationProgramDtoRequest, Principal principal) {
        return new ResponseEntity<>(educationProgramService.createEducationProgram(educationProgramDtoRequest, Long.parseLong(principal.getName())), OK);
    }

    @PreAuthorize("hasAuthority('update-education-program')")
    @PutMapping("/update-education-program")
    public ResponseEntity<HttpStatus> updateEducationProgram(@RequestBody EducationProgramDtoRequest educationProgramDtoRequest,
                                                             @RequestParam(name = "education_program_id") Long educationProgramId,
                                                             Principal principal) {
        educationProgramService.updateEducationProgram(educationProgramDtoRequest, educationProgramId, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasAuthority('delete-education-program')")
    @DeleteMapping("/delete-education-program")
    public ResponseEntity<HttpStatus> deleteEducationProgram(@RequestParam(name = "education_program_id") Long educationProgramId,
                                                             Principal principal) {
        educationProgramService.deleteEducationProgram(educationProgramId, Long.parseLong(principal.getName()));
        return new ResponseEntity<>(OK);
    }
}
