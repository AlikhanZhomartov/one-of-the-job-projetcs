package kz.edu.astanait.astanaitofficemodule.repository.catalog;

import kz.edu.astanait.astanaitofficemodule.model.catalog.AcademicDegree;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcademicDegreeRepository extends JpaRepository<AcademicDegree, Long> {
}
