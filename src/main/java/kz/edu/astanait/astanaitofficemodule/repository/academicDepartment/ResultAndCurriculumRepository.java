package kz.edu.astanait.astanaitofficemodule.repository.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.ResultAndCurriculum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultAndCurriculumRepository extends JpaRepository<ResultAndCurriculum, Long> {
    List<ResultAndCurriculum> findByResultEducationProgramId(Long id);
}
