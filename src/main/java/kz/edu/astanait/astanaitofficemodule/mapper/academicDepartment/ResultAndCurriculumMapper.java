package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.ResultAndCurriculumDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.ResultAndCurriculum;

import java.util.Objects;

public class ResultAndCurriculumMapper {

    public ResultAndCurriculumDtoResponse resultAndCurriculumToDto(ResultAndCurriculum resultAndCurriculum) {
        ResultAndCurriculumDtoResponse resultAndCurriculumDtoResponse = new ResultAndCurriculumDtoResponse();
        resultAndCurriculumDtoResponse.setId(resultAndCurriculum.getId());
        if (Objects.nonNull(resultAndCurriculum.getResult())) resultAndCurriculumDtoResponse.setResult(new ResultOfEducationProgramMapper().resultOfEducationProgramToDto(resultAndCurriculum.getResult()));
        if (Objects.nonNull(resultAndCurriculum.getCurriculum())) resultAndCurriculumDtoResponse.setCurriculum(new CurriculumMapper().curriculumToDto(resultAndCurriculum.getCurriculum()));
        return resultAndCurriculumDtoResponse;
    }
}
