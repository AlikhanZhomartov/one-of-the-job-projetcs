package kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.ResultOfEducationProgram;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ResultOfEducationProgramDtoRequest {

    @NotNull(message = "образовательная программа должна быть выбрана!")
    private Long educationProgramId;

    @NotNull(message = "Результат на английском не может быть пустым!")
    private String resultEn;

    @NotNull(message = "Результат на русском не может быть пустым!")
    private String resultRu;

    @NotNull(message = "Результат на казахском не может быть пустым!")
    private String resultKz;

    @NotNull(message = "Код результата не может быть пустым!")
    private String code;
}
