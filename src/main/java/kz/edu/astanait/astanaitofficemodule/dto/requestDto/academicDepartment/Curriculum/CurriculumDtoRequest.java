package kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.Curriculum;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CurriculumDtoRequest {
    @NotNull(message = "образовательная программа должна быть выбрана!")
    private Long educationProgramId;

    @NotNull(message = "Дисциплина должна быть выбрана!")
    private Long disciplineId;

    @NotNull(message = "Номер триместра должен быть выбран!")
    private Integer numberOfTrimester;
}
