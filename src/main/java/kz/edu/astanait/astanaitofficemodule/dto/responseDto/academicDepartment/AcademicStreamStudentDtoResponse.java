package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student.StudentDtoResponse;
import lombok.Data;

import java.util.List;

@Data
public class AcademicStreamStudentDtoResponse {

    private StudentDtoResponse student;

    private List<AcademicStreamStudentGradeDtoResponse> academicStreamStudentGrade;
}
