package kz.edu.astanait.astanaitofficemodule.controller.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.ResultAndCurriculumDtoResponse;
import kz.edu.astanait.astanaitofficemodule.service.academicDepartment.ResultAndCurriculumService;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/academic-department/result-and-curriculum")
public class ResultAndCurriculumController {

    private final ResultAndCurriculumService resultAndCurriculumService;

    @Autowired
    public ResultAndCurriculumController(ResultAndCurriculumService resultAndCurriculumService) {
        this.resultAndCurriculumService = resultAndCurriculumService;
    }

    @PreAuthorize("hasAuthority('get-result-and-curriculum-by-education-program')")
    @GetMapping("/get-result-and-curriculum-by-education-program")
    public ResponseEntity<List<ResultAndCurriculumDtoResponse>> getResultAndCurriculumByEducationProgram(@RequestParam(name = "education_program_id") Long educationProgramId) {
        return new ResponseEntity<>(resultAndCurriculumService.getAllResultAndCurriculum(educationProgramId), OK);
    }

    @PreAuthorize("hasAuthority('create-result-and-curriculum')")
    @PostMapping("/create-result-and-curriculum")
    public ResponseEntity<Long> createResultAndCurriculum(@RequestParam(name = "result_id") Long resultId, @RequestParam(name = "curriculum_id") Long curriculum_id) {
        return new ResponseEntity<>(resultAndCurriculumService.createResultAndCurriculum(resultId, curriculum_id), OK);
    }

    @PreAuthorize("hasAuthority('delete-result-and-curriculum')")
    @DeleteMapping("/delete-result-and-curriculum")
    public ResponseEntity<HttpStatus> deleteResultAndCurriculum(@RequestParam(name = "result_and_curriculum_id") Long resultAndCurriculumId) {
        resultAndCurriculumService.deleteResultAndCurriculum(resultAndCurriculumId);
        return new ResponseEntity<>(OK);
    }
}
