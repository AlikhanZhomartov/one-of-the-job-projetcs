package kz.edu.astanait.astanaitofficemodule.repositoryHistory.academicDepartmentHistory;

import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.EducationProgramHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EducationProgramHistoryRepository extends JpaRepository<EducationProgramHistory, Long> {
}
