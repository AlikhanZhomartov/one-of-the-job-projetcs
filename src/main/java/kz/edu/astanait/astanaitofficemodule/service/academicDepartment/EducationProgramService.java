package kz.edu.astanait.astanaitofficemodule.service.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.constant.TypeOfActionForHistory;
import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.EducationProgram.EducationProgramDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.EducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionDescription;
import kz.edu.astanait.astanaitofficemodule.exception.domain.CustomNotFoundException;
import kz.edu.astanait.astanaitofficemodule.exception.domain.RepositoryException;
import kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment.EducationProgramMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.EducationProgram;
import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.EducationProgramHistory;
import kz.edu.astanait.astanaitofficemodule.repository.academicDepartment.EducationProgramRepository;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.AcademicDegreeRepository;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.DepartmentRepository;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.EducationLanguageRepository;
import kz.edu.astanait.astanaitofficemodule.repository.catalog.GroupOfEducationProgramRepository;
import kz.edu.astanait.astanaitofficemodule.serviceHistory.academicDepartmentHistory.EducationProgramHistoryService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EducationProgramService {

    private final EducationProgramRepository educationProgramRepository;
    private final DepartmentRepository departmentRepository;
    private final AcademicDegreeRepository academicDegreeRepository;
    private final EducationLanguageRepository educationLanguageRepository;
    private final GroupOfEducationProgramRepository groupOfEducationProgramRepository;
    private final EducationProgramHistoryService educationProgramHistoryService;

    @Autowired
    public EducationProgramService(EducationProgramRepository educationProgramRepository, DepartmentRepository departmentRepository,
                                   AcademicDegreeRepository academicDegreeRepository, EducationLanguageRepository educationLanguageRepository,
                                   GroupOfEducationProgramRepository groupOfEducationProgramRepository,
                                   EducationProgramHistoryService educationProgramHistoryService) {
        this.educationProgramRepository = educationProgramRepository;
        this.departmentRepository = departmentRepository;
        this.academicDegreeRepository = academicDegreeRepository;
        this.educationLanguageRepository = educationLanguageRepository;
        this.groupOfEducationProgramRepository = groupOfEducationProgramRepository;
        this.educationProgramHistoryService = educationProgramHistoryService;
    }

   public EducationProgramDtoResponse getEducationProgramByOne(Long educationProgramId) {
        EducationProgram educationProgram = educationProgramRepository.findById(educationProgramId).orElseThrow(
                () -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Education program", "id")));
        return new EducationProgramMapper().educationProgramToDto(educationProgram);
   }

    public List<EducationProgramDtoResponse> getAllEducationPrograms() {
        return educationProgramRepository.findAll().stream().map(v -> new EducationProgramMapper().educationProgramToDto(v))
                .collect(Collectors.toList());
    }

   public Long createEducationProgram(EducationProgramDtoRequest educationProgramDtoRequest, Long userId) {
        EducationProgram educationProgram = new EducationProgram();
        EducationProgram createdEducationProgram = manipulateEducationProgram(educationProgramDtoRequest, educationProgram, TypeOfActionForHistory.CREATE);
        educationProgramHistoryService.createEducationProgram(createdEducationProgram, userId);
        return createdEducationProgram.getId();
   }

   @Transactional
   public void updateEducationProgram(EducationProgramDtoRequest educationProgramDtoRequest, Long educationProgramId, Long userId) {
        EducationProgram educationProgram = educationProgramRepository.findById(educationProgramId).orElseThrow(
                () -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Education program", "id")));

       educationProgramHistoryService.updateEducationProgram(educationProgram, userId);

        manipulateEducationProgram(educationProgramDtoRequest, educationProgram, TypeOfActionForHistory.UPDATE);
   }

   public void deleteEducationProgram(Long educationProgramId, Long userId) {
       EducationProgram educationProgram = educationProgramRepository.findById(educationProgramId).orElseThrow(
               () -> new CustomNotFoundException(String.format(ExceptionDescription.CustomNotFoundException, "Education program", "id")));
        try {
            educationProgramRepository.deleteById(educationProgramId);
            educationProgramHistoryService.deleteEducationProgram(educationProgram, userId);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "education program", TypeOfActionForHistory.DELETE));
        }
   }

   private EducationProgram manipulateEducationProgram(EducationProgramDtoRequest educationProgramDtoRequest, EducationProgram educationProgram, String action) {
       if (Strings.isNotBlank(educationProgramDtoRequest.getTitleEn())) educationProgram.setTitleEn(educationProgramDtoRequest.getTitleEn());
       if (Strings.isNotBlank(educationProgramDtoRequest.getTitleRu())) educationProgram.setTitleRu(educationProgramDtoRequest.getTitleRu());
       if (Strings.isNotBlank(educationProgramDtoRequest.getTitleKz())) educationProgram.setTitleKz(educationProgramDtoRequest.getTitleKz());
       if (Strings.isNotBlank(educationProgramDtoRequest.getDescriptionEn())) educationProgram.setDescriptionEn(educationProgramDtoRequest.getDescriptionEn());
       if (Strings.isNotBlank(educationProgramDtoRequest.getDescriptionRu())) educationProgram.setDescriptionRu(educationProgramDtoRequest.getDescriptionRu());
       if (Strings.isNotBlank(educationProgramDtoRequest.getDescriptionKz())) educationProgram.setDescriptionKz(educationProgramDtoRequest.getDescriptionKz());
       if (Strings.isNotBlank(educationProgramDtoRequest.getDistinctiveFeaturesEn())) educationProgram.setDistinctiveFeaturesEn(educationProgramDtoRequest.getDistinctiveFeaturesEn());
       if (Strings.isNotBlank(educationProgramDtoRequest.getDistinctiveFeaturesRu())) educationProgram.setDistinctiveFeaturesRu(educationProgramDtoRequest.getDistinctiveFeaturesRu());
       if (Strings.isNotBlank(educationProgramDtoRequest.getDistinctiveFeaturesKz())) educationProgram.setDistinctiveFeaturesKz(educationProgramDtoRequest.getDistinctiveFeaturesKz());
       if (Objects.nonNull(educationProgramDtoRequest.getDepartmentId())) departmentRepository.findById(educationProgramDtoRequest.getDepartmentId()).ifPresent(educationProgram::setDepartment);
       if (Strings.isNotBlank(educationProgramDtoRequest.getTypeOfEducationProgram())) educationProgram.setTypeOfEducationProgram(educationProgramDtoRequest.getTypeOfEducationProgram());
       if (Strings.isNotBlank(educationProgramDtoRequest.getTypeOfResponse())) educationProgram.setTypeOfResponse(educationProgramDtoRequest.getTypeOfResponse());
       if (Strings.isNotBlank(educationProgramDtoRequest.getStatus())) educationProgram.setStatus(educationProgramDtoRequest.getStatus());
       if (Strings.isNotBlank(educationProgramDtoRequest.getLevelByNRK())) educationProgram.setLevelByNRK(educationProgramDtoRequest.getLevelByNRK());
       if (Strings.isNotBlank(educationProgramDtoRequest.getLevelByORK())) educationProgram.setLevelByORK(educationProgramDtoRequest.getLevelByORK());
       if (Objects.nonNull(educationProgramDtoRequest.getAcademicDegreeId())) academicDegreeRepository.findById(educationProgramDtoRequest.getAcademicDegreeId());
       if (Objects.nonNull(educationProgramDtoRequest.getEducationStartedDate())) educationProgram.setEducationStartedDate(educationProgramDtoRequest.getEducationStartedDate());
       if (Objects.nonNull(educationProgramDtoRequest.getVolumeCredits())) educationProgram.setVolumeCredits(educationProgramDtoRequest.getVolumeCredits());
       if (Objects.nonNull(educationProgramDtoRequest.getEducationLanguageId())) educationLanguageRepository.findById(educationProgramDtoRequest.getEducationLanguageId()).ifPresent(educationProgram::setEducationLanguage);
       if (Objects.nonNull(educationProgramDtoRequest.getGroupOfEducationProgramId())) groupOfEducationProgramRepository.findById(educationProgramDtoRequest.getGroupOfEducationProgramId()).ifPresent(educationProgram::setGroupOfEducationProgram);
       if (Objects.nonNull(educationProgramDtoRequest.getApprovalDateByAcademicCouncil())) educationProgram.setApprovalDateByAcademicCouncil(educationProgramDtoRequest.getApprovalDateByAcademicCouncil());
       if (Objects.nonNull(educationProgramDtoRequest.getLicense())) educationProgram.setLicense(educationProgramDtoRequest.getLicense());
       if (Objects.nonNull(educationProgramDtoRequest.getAccreditation())) educationProgram.setAccreditation(educationProgramDtoRequest.getAccreditation());
       try {
          return educationProgramRepository.save(educationProgram);
       } catch (Exception e) {
           e.printStackTrace();
           throw new RepositoryException(String.format(ExceptionDescription.RepositoryException, "education program", action));
       }
   }
}
