package kz.edu.astanait.astanaitofficemodule.controller.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.ResultOfEducationProgram.ResultOfEducationProgramDtoRequest;
import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.ResultOfEducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionHandling;
import kz.edu.astanait.astanaitofficemodule.service.academicDepartment.ResultOfEducationProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/academic-department/result-of-education-program")
public class ResultOfEducationProgramController extends ExceptionHandling {

    private final ResultOfEducationProgramService resultOfEducationProgramService;

    @Autowired
    public ResultOfEducationProgramController(ResultOfEducationProgramService resultOfEducationProgramService) {
        this.resultOfEducationProgramService = resultOfEducationProgramService;
    }

    @PreAuthorize("hasAuthority('get-results-of-education-program')")
    @GetMapping("/get-results-of-education-program")
    public ResponseEntity<List<ResultOfEducationProgramDtoResponse>> getResultOfEducationProgram(@RequestParam(name = "education_program_id") Long educationProgramId) {
        return new ResponseEntity<>(resultOfEducationProgramService.getResultOfEducationProgram(educationProgramId), OK);
    }

    @PreAuthorize("hasAuthority('create-result-of-education-program')")
    @PostMapping("/create-result-of-education-program")
    public ResponseEntity<HttpStatus> createResultOfEducationProgram(@Valid @RequestBody ResultOfEducationProgramDtoRequest resultOfEducationProgramDtoRequests) {
        resultOfEducationProgramService.createResultOfEducationProgram(resultOfEducationProgramDtoRequests);
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasAuthority('update-result-of-education-program')")
    @PutMapping("/update-result-of-education-program")
    public ResponseEntity<HttpStatus> updateResultOfEducationProgram(@RequestBody ResultOfEducationProgramDtoRequest resultOfEducationProgramDtoRequests,
                                                                     @RequestParam(name = "result_of_education_program_id") Long resultOfEducationProgramId) {
        resultOfEducationProgramService.updateResultOfEducationProgram(resultOfEducationProgramDtoRequests, resultOfEducationProgramId);
        return new ResponseEntity<>(OK);
    }

    @PreAuthorize("hasAuthority('delete-results-of-education-program')")
    @DeleteMapping("/delete-results-of-education-program")
    public ResponseEntity<HttpStatus> deleteResultOfEducationProgram(@RequestParam(name = "education_program_id") Long educationProgramId) {
        resultOfEducationProgramService.deleteResultOfEducationProgram(educationProgramId);
        return new ResponseEntity<>(OK);
    }

}
