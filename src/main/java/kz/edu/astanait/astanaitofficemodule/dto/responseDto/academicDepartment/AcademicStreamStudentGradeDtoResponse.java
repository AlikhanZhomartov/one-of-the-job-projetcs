package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AcademicStreamStudentGradeDtoResponse {

    private Float grade;

    private String gradeType;

    private Boolean approved;

    private LocalDateTime createdDate;
}
