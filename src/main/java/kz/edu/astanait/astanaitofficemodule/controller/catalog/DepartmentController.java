package kz.edu.astanait.astanaitofficemodule.controller.catalog;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.DepartmentDtoResponse;
import kz.edu.astanait.astanaitofficemodule.exception.ExceptionHandling;
import kz.edu.astanait.astanaitofficemodule.service.catalogService.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.HttpStatus.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/catalog/department")
public class DepartmentController extends ExceptionHandling {

    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @PreAuthorize("hasAuthority('get-all-departments')")
    @GetMapping("/get-all-departments")
    public ResponseEntity<List<DepartmentDtoResponse>> getAllDepartments(){
        List<DepartmentDtoResponse> departmentDtoResponseList = departmentService.getAllDepartments();
        return new ResponseEntity<>(departmentDtoResponseList, OK);
    }


}
