package kz.edu.astanait.astanaitofficemodule.dto.feignClient.responseDto.student;

import lombok.Data;

@Data
public class GroupDtoResponse {
    private Long id;

    private String title;
}
