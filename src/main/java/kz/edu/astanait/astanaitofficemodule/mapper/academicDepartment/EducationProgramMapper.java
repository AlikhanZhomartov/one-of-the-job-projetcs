package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.EducationProgramDtoResponse;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.AcademicDegreeMapper;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.DepartmentMapper;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.EducationLanguageMapper;
import kz.edu.astanait.astanaitofficemodule.mapper.catalog.GroupOfEducationProgramMapper;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.EducationProgram;
import org.apache.logging.log4j.util.Strings;

import java.util.Objects;

public class EducationProgramMapper {

    public EducationProgramDtoResponse educationProgramToDto(EducationProgram educationProgram) {
        EducationProgramDtoResponse educationProgramDtoResponse = new EducationProgramDtoResponse();
        educationProgramDtoResponse.setId(educationProgram.getId());
        if (Strings.isNotBlank(educationProgram.getTitleEn())) educationProgramDtoResponse.setTitleEn(educationProgram.getTitleEn());
        if (Strings.isNotBlank(educationProgram.getTitleRu())) educationProgramDtoResponse.setTitleRu(educationProgram.getTitleRu());
        if (Strings.isNotBlank(educationProgram.getTitleKz())) educationProgramDtoResponse.setTitleKz(educationProgram.getTitleKz());
        if (Strings.isNotBlank(educationProgram.getDescriptionEn())) educationProgramDtoResponse.setDescriptionEn(educationProgram.getDescriptionEn());
        if (Strings.isNotBlank(educationProgram.getDescriptionRu())) educationProgramDtoResponse.setDescriptionRu(educationProgram.getDescriptionRu());
        if (Strings.isNotBlank(educationProgram.getDescriptionKz())) educationProgramDtoResponse.setDescriptionKz(educationProgram.getDescriptionKz());
        if (Strings.isNotBlank(educationProgram.getDistinctiveFeaturesEn())) educationProgramDtoResponse.setDistinctiveFeaturesEn(educationProgram.getDistinctiveFeaturesEn());
        if (Strings.isNotBlank(educationProgram.getDistinctiveFeaturesRu())) educationProgramDtoResponse.setDistinctiveFeaturesRu(educationProgram.getDistinctiveFeaturesRu());
        if (Strings.isNotBlank(educationProgram.getDistinctiveFeaturesKz())) educationProgramDtoResponse.setDistinctiveFeaturesKz(educationProgram.getDistinctiveFeaturesKz());
        if (Objects.nonNull(educationProgram.getDepartment())) educationProgramDtoResponse.setDepartment(new DepartmentMapper().departmentToDto(educationProgram.getDepartment()));
        if (Strings.isNotBlank(educationProgram.getTypeOfEducationProgram())) educationProgramDtoResponse.setTypeOfEducationProgram(educationProgram.getTypeOfEducationProgram());
        if (Strings.isNotBlank(educationProgram.getTypeOfResponse())) educationProgramDtoResponse.setTypeOfResponse(educationProgram.getTypeOfResponse());
        if (Strings.isNotBlank(educationProgram.getStatus())) educationProgramDtoResponse.setStatus(educationProgram.getStatus());
        if (Strings.isNotBlank(educationProgram.getLevelByNRK())) educationProgramDtoResponse.setLevelByNRK(educationProgram.getLevelByNRK());
        if (Strings.isNotBlank(educationProgram.getLevelByORK())) educationProgramDtoResponse.setLevelByORK(educationProgram.getLevelByORK());
        if (Objects.nonNull(educationProgram.getAcademicDegree())) educationProgramDtoResponse.setAcademicDegree(new AcademicDegreeMapper().academicDegreeToDto(educationProgram.getAcademicDegree()));
        if (Objects.nonNull(educationProgram.getEducationStartedDate())) educationProgramDtoResponse.setEducationStartedDate(educationProgram.getEducationStartedDate());
        if (Objects.nonNull(educationProgram.getVolumeCredits())) educationProgramDtoResponse.setVolumeCredits(educationProgram.getVolumeCredits());
        if (Objects.nonNull(educationProgram.getEducationStartedDate())) educationProgramDtoResponse.setEducationStartedDate(educationProgram.getEducationStartedDate());
        if (Objects.nonNull(educationProgram.getEducationLanguage())) educationProgramDtoResponse.setEducationLanguage(new EducationLanguageMapper().educationLanguageToDto(educationProgram.getEducationLanguage()));
        if (Objects.nonNull(educationProgram.getGroupOfEducationProgram())) educationProgramDtoResponse.setGroupOfEducationProgram(new GroupOfEducationProgramMapper().groupOfEducationProgramToDto(educationProgram.getGroupOfEducationProgram()));
        if (Objects.nonNull(educationProgram.getApprovalDateByAcademicCouncil())) educationProgramDtoResponse.setApprovalDateByAcademicCouncil(educationProgram.getApprovalDateByAcademicCouncil());
        if (Objects.nonNull(educationProgram.getLicense())) educationProgramDtoResponse.setLicense(educationProgram.getLicense());
        if (Objects.nonNull(educationProgram.getAccreditation())) educationProgramDtoResponse.setAccreditation(educationProgram.getAccreditation());
        return educationProgramDtoResponse;
    }
}
