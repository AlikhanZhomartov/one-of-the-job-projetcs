package kz.edu.astanait.astanaitofficemodule.mapper.catalog;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.catalog.DepartmentDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.catalog.Department;
import org.apache.logging.log4j.util.Strings;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
public class DepartmentMapper {

    public DepartmentDtoResponse departmentToDto(Department department) {
        DepartmentDtoResponse departmentDtoResponse = new DepartmentDtoResponse();
        departmentDtoResponse.setId(department.getId());
        if (Strings.isNotBlank(department.getTitleEn())) departmentDtoResponse.setTitleEn(department.getTitleEn());
        if (Strings.isNotBlank(department.getTitleRu())) departmentDtoResponse.setTitleRu(department.getTitleRu());
        if (Strings.isNotBlank(department.getTitleKz())) departmentDtoResponse.setTitleKz(department.getTitleKz());
        return departmentDtoResponse;
    }
}
