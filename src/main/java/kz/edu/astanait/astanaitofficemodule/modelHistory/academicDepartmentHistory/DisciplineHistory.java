package kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory;


import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "z_discipline_history")
public class DisciplineHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "discipline_id")
    private Long disciplineId;

    @Column(name = "user_id")
    private Long user_id;

    @Column(name = "title_en")
    private String titleEn;

    @Column(name = "title_ru")
    private String titleRu;

    @Column(name = "title_kz")
    private String titleKz;

    @Column(name = "description_en")
    private String descriptionEn;

    @Column(name = "description_ru")
    private String descriptionRu;

    @Column(name = "description_kz")
    private String descriptionKz;

    @Column(name = "code")
    private String code;

    @Column(name = "volume_credits")
    private Integer volumeCredits;

    @Column(name = "department_id")
    private Long departmentId;

    @Column(name = "academic_degree_id")
    private Long academicDegreeId;

    @Column(name = "type_of_action")
    private String typeOfAction;

    @Column(name = "created_date")
    private LocalDateTime createdDate;

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }
}
