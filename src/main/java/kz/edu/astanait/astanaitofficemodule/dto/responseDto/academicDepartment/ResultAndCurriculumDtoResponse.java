package kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment;

import lombok.Data;


@Data
public class ResultAndCurriculumDtoResponse {
    private Long id;

    private ResultOfEducationProgramDtoResponse result;

    private CurriculumDtoResponse curriculum;
}
