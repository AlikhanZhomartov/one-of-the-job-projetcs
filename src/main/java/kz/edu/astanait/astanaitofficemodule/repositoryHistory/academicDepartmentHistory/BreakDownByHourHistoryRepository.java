package kz.edu.astanait.astanaitofficemodule.repositoryHistory.academicDepartmentHistory;

import kz.edu.astanait.astanaitofficemodule.modelHistory.academicDepartmentHistory.BreakDownByHourHistory;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BreakDownByHourHistoryRepository extends JpaRepository<BreakDownByHourHistory, Long> {
}
