package kz.edu.astanait.astanaitofficemodule.dto.requestDto.academicDepartment.EducationProgram;
import lombok.Data;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;

@Data
public class EducationProgramDtoRequest {

    @NotBlank(message = "Название на английском не может быть пустым!")
    private String titleEn;

    @NotBlank(message = "Название на русском не может быть пустым!")
    private String titleRu;

    @NotBlank(message = "Название на казахском не может быть пустым!")
    private String titleKz;

    @NotBlank(message = "Описание на английском не может быть пустым!")
    private String descriptionEn;

    @NotBlank(message = "Описание на русском не может быть пустым!")
    private String descriptionRu;

    @NotBlank(message = "Описание на казахском не может быть пустым!")
    private String descriptionKz;

    @NotBlank(message = "Описание отличительных черт на английском не может быть пустым!")
    private String distinctiveFeaturesEn;

    @NotBlank(message = "Описание отличительных черт на русском не может быть пустым!")
    private String distinctiveFeaturesRu;

    @NotBlank(message = "Описание отличительных черт на казахском не может быть пустым!")
    private String distinctiveFeaturesKz;

    @NotNull(message = "Департамент должен быть выбран!")
    private Long departmentId;

    @NotBlank(message = "Тип образовательной программы не может быть пустым!")
    private String typeOfEducationProgram;

    @NotBlank(message = "Тип заявки не может быть пустым!")
    private String typeOfResponse;

    @NotBlank(message = "Статус не может быть пустым!")
    private String status;

    @NotBlank(message = "Уровень по НРК не может быть пустым!")
    private String levelByNRK;

    @NotBlank(message = "Уровень по ОРК не может быть пустым!")
    private String levelByORK;

    @NotNull(message = "Академическая степень должна быть выбрана!")
    private Long academicDegreeId;

    @NotNull(message = "Дата начала учебного года должна быть выбрана!")
    private LocalDate educationStartedDate;

    @NotNull(message = "Количество кредитов не должно быть нулевым!")
    private Integer volumeCredits;

    @NotNull(message = "Язык обучения должен быть выбран!")
    private Long educationLanguageId;

    @NotNull(message = "Группа образовательных программ должна быть выбрев!")
    private Long groupOfEducationProgramId;

    private LocalDate approvalDateByAcademicCouncil;

    @NotNull(message = "Статус лицензии должен быть указан!")
    private Boolean license;

    @NotNull(message = "Статус аккредитации должен быть указан!")
    private Boolean accreditation;
}
