package kz.edu.astanait.astanaitofficemodule.mapper.academicDepartment;

import kz.edu.astanait.astanaitofficemodule.dto.responseDto.academicDepartment.PreviousCurriculumDisciplineDtoResponse;
import kz.edu.astanait.astanaitofficemodule.model.academicDepartment.PreviousCurriculumDiscipline;

import java.util.Objects;

public class PreviousCurriculumDisciplineMapper {

    public PreviousCurriculumDisciplineDtoResponse previousCurriculumDisciplineToDto(PreviousCurriculumDiscipline previousCurriculumDiscipline) {
        PreviousCurriculumDisciplineDtoResponse previousCurriculumDisciplineDtoResponse = new PreviousCurriculumDisciplineDtoResponse();
        previousCurriculumDisciplineDtoResponse.setId(previousCurriculumDiscipline.getId());
        if (Objects.nonNull(previousCurriculumDiscipline.getCurriculum())) previousCurriculumDisciplineDtoResponse.setCurriculum(new CurriculumMapper().curriculumToDto(previousCurriculumDiscipline.getCurriculum()));
        if (Objects.nonNull(previousCurriculumDiscipline.getPreviousCurriculumDiscipline())) previousCurriculumDisciplineDtoResponse.setPreviousCurriculumDiscipline(new CurriculumMapper().curriculumToDto(previousCurriculumDiscipline.getPreviousCurriculumDiscipline()));
        return previousCurriculumDisciplineDtoResponse;
    }
}
